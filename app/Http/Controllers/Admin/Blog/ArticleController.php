<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Blog\Article;
use App\Models\Blog\Category;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use App\Models\Blog\Tag;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('back.blog.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $autors = User::where('role', '!=', 0)->get();
        $categories = Category::all();
        return view('back.blog.articles.create', compact('autors', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input());
        $request->validate([
            'title' => 'required|min:4|max:60',
            'slug' => 'required|alpha_dash|unique:articles',
            'content' => 'required',
            'description' => 'required|min:50|max:600',
            'image' => 'required|image|max:2048|dimensions:min_width:800,min_height:200',
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:article_categories,id'
        ]);

        $article = new Article($request->input());
        if($request->hasFile('image')){
            $imageName = 'ba_'.str_random(rand(10, 23)).'.jpg';
            Image::make($request->file('image'))->save('uploads/article/'.$imageName);
            $article->image = $imageName;
        }

        if($request->has('published')) $article->published = true; else $article->published = false;

        $article->save();

        if($request->has('tags')){
            $tags = explode(',', $request->input('tags'));
            foreach($tags as $tag){
                if(Tag::where('id', $tag)->exists()) $article->tags()->attach($tag);
            }
            $article->save();
        }

        return redirect()->route('admin.blog.articles.index')->with('adminBlogArticleCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $article->published = !$article->published;
        $article->save();
        return redirect()->route('admin.blog.articles.index')->with('adminBlogArticleEditedSuccess', true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $autors = User::where('role', '!=', 0)->get();
        $categories = Category::all();
        $tags = Tag::all();
        return view('back.blog.articles.edit', compact('autors', 'article', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title' => 'required|min:4|max:60',
            'slug' => ['required', 'alpha_dash', Rule::unique('articles')->ignore($article->id)],
            'content' => 'required',
            'description' => 'required|min:50|max:600',
            'image' => 'image|max:2048|dimensions:min_width:800,min_height:200',
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:article_categories,id'
        ]);

        $article->title = $request->input('title');
        $article->slug = $request->input('slug');
        $article->content = $request->input('content');
        $article->description = $request->input('description');
        $article->user_id = $request->input('user_id');
        $article->category_id = $request->input('category_id');

        if($request->hasFile('image')){
            if ( file_exists( $article->getImage()) ){
                File::delete($article->getImage());
            }
            $imageName = 'ba_'.str_random(rand(10, 23)).'.jpg';
            Image::make($request->file('image'))->save('uploads/article/'.$imageName);
            $article->image = $imageName;
        }

        if($request->has('tags')){
            $tags = explode(',', $request->input('tags'));
            foreach($tags as $tag){
                if(Tag::where('id', $tag)->exists()) $article->tags()->attach($tag);
            }
        }

        if($request->has('published')) $article->published = true; else $article->published = false;

        $article->save();

        return redirect()->route('admin.blog.articles.index')->with('adminBlogArticleUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        // TODO: Suppresion de l'avatar, des commentaires ...
        $article->tags()->detach();
        $article->delete();
        return redirect()->back()->with('adminBlogArticleDeletedSuccess', true);
    }
}
