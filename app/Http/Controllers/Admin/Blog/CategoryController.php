<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Blog\Category;
use App\Models\Icon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('back.blog.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $icons = Icon::all();
        return view('back.blog.categories.create', compact('icons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'icon_id' => 'required|exists:icons,id'
        ]);

        $category = new Category($request->input());
        $category->save();

        return redirect()->route('admin.blog.categories.index')->with('adminBlogCategoryCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $icons = Icon::all();
        return view('back.blog.categories.edit', compact('category', 'icons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required|min:3',
            'icon_id' => 'required|exists:icons,id'
        ]);

        $category->name = $request->input('name');
        $category->icon_id = $request->input('icon_id');
        $category->save();

        return redirect()->route('admin.blog.categories.index')->with('adminBlogCategoryUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('admin.blog.categories.index')->with('adminBlogCategoryDeletedSuccess', true);
    }
}
