<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Models\Blog\Article;
use App\Models\Blog\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::all();
        return view('back.blog.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.blog.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'slug' => 'required|min:3|alpha_dash|unique:article_tags'
        ]);

        $tag = new Tag($request->input());
        $tag->save();

        return redirect()->route('admin.blog.tags.index')->with('adminBlogTagCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        return view('back.blog.tags.show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('back.blog.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $request->validate([
            'slug' => ['required', 'min' => 3, 'alpha_dash', Rule::unique('article_tags')->ignore($tag->id)]
        ]);

        $tag->slug = $request->input('slug');
        $tag->save();

        return redirect()->route('admin.blog.tags.index')->with('adminBlogTagUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        return redirect()->route('admin.blog.tags.index')->with('adminBlogTagDeletedSuccess', true);
    }

    public function json(Request $request){
        $tags = Tag::all();
        return json_encode($tags);
    }
}
