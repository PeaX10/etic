<?php

namespace App\Http\Controllers\Admin\Categorization;

use App\Models\Categorization\ActivityArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ActivityAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $parent = Input::get('parent');
        if(!is_numeric($parent)) $parent = null;

        if(ActivityArea::where('id', $parent)->exists()|| $parent == null){
            if($parent != null) {
                $parent = ActivityArea::find($parent);
                $activityAreas = ActivityArea::where('parent_id', $parent->id)->get();
            }else{
                $activityAreas = ActivityArea::where('parent_id', $parent)->get();
            }
            return view('back.categorization.activity_areas.index', compact('activityAreas', 'parent'));
        }else{
            abort(404);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent = Input::get('parent');
        if(!is_numeric($parent)){
            $parent = null;
        }else{
            $parent = ActivityArea::find($parent);
        }

        return view('back.categorization.activity_areas.create', compact('activityAreas', 'parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'nullable|exists:activity_areas,id'
        ]);

        $activityArea = new ActivityArea($request->input());
        $activityArea->save();

        $param = [];
        if($request->has('parent_id')){
            $param['parent'] = $request->input('parent_id');
        }

        return redirect()->route('admin.categorization.activity_areas.index', $param)->with('adminCategorizationActivityAreaDeletedSuccess', true);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityArea $activityArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityArea $activityArea)
    {
        return view('back.categorization.activity_areas.edit', compact('activityArea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityArea $activityArea)
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'nullable|exists:activity_areas,id'
        ]);

        $activityArea->update($request->input());

        $param = [];
        if($request->has('parent_id')){
            $param['parent'] = $request->input('parent_id');
        }

        return redirect()->route('admin.categorization.activity_areas.index', $param)->with('adminCategorizationActivityAreaUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActivityArea  $activityArea
     * @param bool $redirect
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityArea $activityArea, bool $redirect)
    {
        // Deleting on cascade
        $childs = ActivityArea::where('parent_id', $activityArea->id)->get();
        foreach($childs as $child){
            $this->destroy($child, false);
        }
        $activityArea->delete();

        if($redirect) return redirect()->back()->with('adminCategorizationActivityAreaDeletedSuccess', true);
    }
}
