<?php

namespace App\Http\Controllers\Admin;

use App\Models\Icon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class IconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icons = Icon::all();
        return view('back.icons.index', compact('icons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.icons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cssClass' => 'required|unique:icons',
        ]);

        $icon = new Icon($request->input());
        $icon->save();

        return redirect()->route('admin.icons.index')->with('adminIconCreatedSuccess', true);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Icon  $icon
     * @return \Illuminate\Http\Response
     */
    public function edit(Icon $icon)
    {
        return view('back.icons.edit', compact('icon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Icon  $icon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Icon $icon)
    {
        $request->validate([
            'cssClass' => ['required', Rule::unique('icons')->ignore($icon->id)],
        ]);

        $icon->cssClass = $request->input('cssClass');
        $icon->save();

        return redirect()->route('admin.icons.index')->with('adminIconUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Icon  $icon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Icon $icon)
    {
        $icon->delete();
        return redirect()->route('admin.blog.articles.index')->with('adminIconDeletedSuccess', true);
    }
}
