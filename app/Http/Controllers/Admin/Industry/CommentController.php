<?php

namespace App\Http\Controllers\Admin\Industry;

use App\Models\Industry\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::all();
        return view('back.industry.comments.index', compact('comments'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Industry\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Industry\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('back.industry.comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $request->validate([
            'username' => 'nullable',
            'email' => 'nullable|email',
            'website' => 'nullable|url',
            'content' => 'required'
        ]);

        $comment->update($request->input());

        return redirect()->route('admin.industry.comments.index')->with('adminIndustryCommentUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Industry\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return redirect()->back()->with('adminIndustryCommentDeletedSuccess', true);
    }
}
