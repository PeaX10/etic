<?php

namespace App\Http\Controllers\Admin\Industry;

use App\Models\Industry\Company;
use App\Service\Wikipedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('back.industry.companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('back.industry.companies.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|image|max:2048',
            'good_observation' => 'nullable',
            'avg_observation' => 'nullable',
            'bad_observation' => 'nullable',
            'parent_id' => 'nullable'
        ]);

        $company = new Company($request->input());

        $imageName = 'ic_'.str_random(rand(10, 23)).'.png';
        Image::make($request->file('image'))->save('uploads/company/'.$imageName);
        $company->image = $imageName;

        if($request->has('infoNames') && $request->has('infoDatas')){
            $infos = array_combine($request->input('infoNames'), $request->input('infoDatas'));
            foreach ($infos as $k => $info) {
                if (empty($k) or empty($info)) unset($infos[$k]);
            }
        }else{
            $infos = [];
        }

        $company->infos = json_encode($infos);

        if($request->has('shareHolders')){
            $shareHolders = $request->input('shareHolders');
            foreach ($shareHolders as $k => $shareHolder){
                if(empty($shareHolder)) unset($shareHolders[$k]);
            }

        }else{
            $shareHolders = [];
        }
        $company->shareHolders = json_encode($shareHolders);

        $company->save();

        return redirect()->route('admin.industry.companies.index')->with('adminInsdustryCompaniesCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Industry\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $companies =  Company::where('parent_id', $company->id)->get();
        return view('back.industry.companies.index', compact('companies', 'company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Industry\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $companies = Company::all();
        return view('back.industry.companies.edit', compact('company', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Industry\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'image|max:2048|nullable',
            'good_observation' => 'nullable',
            'avg_observation' => 'nullable',
            'bad_observation' => 'nullable',
            'parent_id' => 'nullable'
        ]);

        if($request->has('infoNames') && $request->has('infoDatas')){
            $infos = array_combine($request->input('infoNames'), $request->input('infoDatas'));
            foreach ($infos as $k => $info){
                if(empty($k) or empty($info)) unset($infos[$k]);
            }
        }else{
            $infos = [];
        }
        $company->infos = json_encode($infos);

        if($request->has('shareHolders')){
            $shareHolders = $request->input('shareHolders');
            foreach ($shareHolders as $k => $shareHolder){
                if(empty($shareHolder)) unset($shareHolders[$k]);
            }
        }else{
            $shareHolders = [];
        }
        $company->shareHolders = json_encode($shareHolders);

        if($request->hasFile('image')){
            Storage::delete($company->getImage());
            $imageName = 'ic_'.str_random(rand(10, 23)).'.png';
            Image::make($request->file('image'))->save('uploads/company/'.$imageName);
            $company->image = $imageName;
        }

        $company ->update($request->input());
        $company->save();

        return redirect()->route('admin.industry.companies.index')->with('adminInsdustryCompaniesUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Industry\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect()->back()->with('adminInsdustryCompaniesDeletedSuccess', true);
    }
}
