<?php

namespace App\Http\Controllers\Admin;

use App\Models\Algo\Coefficient;
use App\Service\Wikipedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function index(){
        return view('back.index');
    }

    public function algorithme(){
        $coefficients = Coefficient::all();
        return view('back.algorithme.index', compact('coefficients'));
    }

    public function postAlgorithme(Request $request){
        $request->validate([
            'sliderHealth' => 'required|between:0,100',
            'sliderEcology' => 'required|between:0,100',
            'sliderHumanressource' => 'required|between:0,100',
            'sliderEconomy' => 'required|between:0,100',
            'sliderProduction' => 'required|between:0,100',
            'sliderDomain' => 'required|between:0,100',
            'sliderSubdomain' => 'required|between:0,100',
            'sliderQuestion' => 'required|between:0,100',
        ]);

        foreach($request->only('sliderHealth', 'sliderEcology', 'sliderHumanressource', 'sliderEconomy', 'sliderProduction', 'sliderDomain', 'sliderSubdomain', 'sliderQuestion') as $coefficient => $value){
            $name = strtolower( str_replace("slider", "", $coefficient) );
            $coefficient = Coefficient::where('name', $name)->first();
            if($coefficient != null){
                $coefficient->update(['value' => $value]);
            }else{
                $coefficient = new Coefficient(['name' => $name, 'value' => $value]);
                $coefficient->save();
            }
        }

        return redirect()->route('admin.algorithme')->with('adminCoefficientCreated', true);
    }

    public function wikipediaApi(Wikipedia $wiki, $object)
    {
        echo json_encode($wiki->getInfoBox($object));
    }
}
