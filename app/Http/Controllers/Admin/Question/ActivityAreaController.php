<?php

namespace App\Http\Controllers\Admin\Question;

use App\Models\Question\ActivityArea;
use App\Service\Form;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = ActivityArea::all();
        return view('back.question.activity_areas.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Form $form)
    {
        $activityAreas = $form->generateTree(\App\Models\Categorization\ActivityArea::class);
        return view('back.question.activity_areas.create', compact('activityAreas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'coefficient' => 'required|between:0,100',
            'activityArea' => 'required|exists:activity_areas,id'
        ]);

        $question = new ActivityArea();
        $question->question = $request->input('question');
        $question->activity_area_id = $request->input('activityArea');
        $question->coefficient = $request->input('coefficient');
        if($request->has('direction') && !empty($request->input('direction'))){
            $question->direction = 1;
        }else{
            $question->direction = -1;
        }

        $question->save();

        return redirect()->route('admin.question.activity_areas.index')->with('adminQuestionActivityAreaCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function show(ActivityArea $activityArea)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function edit(ActivityArea $activityArea, Form $form)
    {
        $activityAreas = $form->generateTree(\App\Models\Categorization\ActivityArea::class);
        return view('back.question.activity_areas.edit', compact('activityAreas', 'activityArea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ActivityArea $activityArea)
    {
        $request->validate([
            'question' => 'required',
            'coefficient' => 'required|between:0,100',
            'activityArea' => 'required|exists:activity_areas,id'
        ]);

        $activityArea->question = $request->input('question');
        $activityArea->activity_area_id = $request->input('activityArea');
        $activityArea->coefficient = $request->input('coefficient');
        if($request->has('direction') && !empty($request->input('direction'))){
            $activityArea->direction = 1;
        }else{
            $activityArea->direction = -1;
        }

        $activityArea->save();

        return redirect()->route('admin.question.activity_areas.index')->with('adminQuestionActivityAreaUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question\ActivityArea  $activityArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityArea $activityArea)
    {
        $activityArea->delete();
        return redirect()->back()->with('adminQuestionActivityAreaDeletedSuccess', true);
    }
}
