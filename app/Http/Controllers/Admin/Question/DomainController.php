<?php

namespace App\Http\Controllers\Admin\Question;

use App\Models\Question\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Domain::all();
        return view('back.question.domains.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $domains = \App\Models\Categorization\Domain::all()->pluck('name', 'id');
        return view('back.question.domains.create', compact('domains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question' => 'required',
            'coefficient' => 'required|between:0,100',
            'domain' => 'required|numeric'
        ]);

        $domainQuestion = new Domain();
        $domainQuestion->question = $request->input('question');
        $domainQuestion->domain_id = $request->input('domain');
        $domainQuestion->coefficient = $request->input('coefficient');
        if($request->has('direction') && !empty($request->input('direction'))){
            $domainQuestion->direction = 1;
        }else{
            $domainQuestion->direction = -1;
        }

        $domainQuestion->save();

        return redirect()->route('admin.question.domains.index')->with('adminQuestionDomainCreatedSuccess', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function edit(Domain $domain)
    {
        $domains = \App\Models\Categorization\Domain::all()->pluck('name', 'id');
        return view('back.question.domains.edit', compact('domains', 'domain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question\Domain  $domainQuestion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Domain $domain)
    {
        $request->validate([
            'question' => 'required',
            'coefficient' => 'required|between:0,100',
            'domain' => 'required|numeric'
        ]);

        $domain->question = $request->input('question');
        $domain->domain_id = $request->input('domain');
        $domain->coefficient = $request->input('coefficient');
        if($request->has('direction') && !empty($request->input('direction'))){
            $domain->direction = 1;
        }else{
            $domain->direction = -1;
        }

        $domain->save();

        return redirect()->route('admin.question.domains.index')->with('adminQuestionDomainUpdatedSuccess', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question\Domain  $domain
     * @return \Illuminate\Http\Response
     */
    public function destroy(Domain $domain)
    {
        $domain->delete();
        return redirect()->back()->with('adminQuestionDomainDeletedSuccess', true);
    }
}
