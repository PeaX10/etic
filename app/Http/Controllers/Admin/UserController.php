<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('back.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => 'required|min:4|alpha_dash|unique:users,name',
            'email' => 'required|email|unique:users',
            'pass1' => 'required|min:6',
            'pass2' => 'required|same:pass1',
            'avatar' => 'image|max:2048|dimensions:min_width:150,min_height:150',
            'role' => 'required|numeric|between:-1,0',
            'fb_id' => '',
            'xp' => 'min:0'
        ]);

        $user = new User();
        $user->name = $request->input('username');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('pass1'));
        $user->role = $request->input('role');
        if(!empty($request->input('xp'))) $user->xp = $request->input('xp');
        $user->remember_token = $request->input('_token');
        if(is_numeric($request->input('fb_id'))) $user->fb_id = $request->input('fb_id');

        if($request->hasFile('avatar')){
            $image = Image::make($request->file('avatar'))->fit(150,150);
            $nameExist = true;
            while($nameExist){
                $avatarName = 'a_'.str_random(rand(6,12)).'.jpg';
                if ( !file_exists( public_path('uploads/avatar/'.$avatarName)) ) $nameExist = false;
            }
            $image->save('uploads/avatar/'.$avatarName);
            $user->avatar = $avatarName;
        }

        $user->save();

        return redirect()->route('admin.users.index')->with('adminUserCreatedSuccess', true);
    }

    /**
     * Log-in as user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        Auth::login($user);
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $eUser = User::findOrFail($id);
        return view('back.users.edit', compact('eUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uUser = User::findOrFail($id);
        $request->validate([
            'name' => ['required', 'min:4', 'alpha_dash', Rule::unique('users')->ignore($uUser->id)],
            'email' => ['required', 'email', Rule::unique('users')->ignore($uUser->id)],
            'pass1' => 'nullable|min:6',
            'pass2' => 'same:pass1',
            'avatar' => 'nullable|image|max:2048|dimensions:min_width:150,min_height:150',
            'role' => 'required|numeric|between:-1,0',
            'fb_id' => 'nullable',
            'xp' => 'nullable|numeric|min:0'
        ]);


        $uUser->name = $request->input('name');
        $uUser->email = $request->input('email');
        if(!empty($request->input('pass1'))) $uUser->password = Hash::make($request->input('pass1'));
        $uUser->role = $request->input('role');
        if(!empty($request->input('xp'))) $uUser->xp = $request->input('xp');
        $uUser->remember_token = $request->input('_token');
        if(is_numeric($request->input('fb_id'))) $uUser->fb_id = $request->input('fb_id');

        if($request->hasFile('avatar')){
            if ( file_exists( public_path('uploads/avatar/'.$uUser->avatar)) ){
                File::delete(public_path('uploads/avatar/'.$uUser->avatar));
            }
            $image = Image::make($request->file('avatar'))->fit(150,150);
            $nameExist = true;
            while($nameExist){
                $avatarName = 'a_'.str_random(rand(6,12)).'.jpg';
                if ( !file_exists( public_path('uploads/avatar/'.$avatarName) ) ) $nameExist = false;
            }
            $image->save('uploads/avatar/'.$avatarName);
            $uUser->avatar = $avatarName;
        }

        $uUser->save();

        return redirect()->route('admin.users.index')->with('adminUserUpdatedSuccess', true);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->id != Auth::user()->id){
            // TODO: Suppresion de l'avatar, des commentaires ...
            $user->delete();
            return redirect()->back()->with('adminUserDeletedSuccess', true);
        }

        return redirect()->back()->with('adminUserDeletedError', true);
    }
}
