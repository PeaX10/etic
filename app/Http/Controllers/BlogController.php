<?php

namespace App\Http\Controllers;

use App\Models\Blog\Comment;
use Illuminate\Http\Request;
use App\Models\Blog\Article;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    private $articlePerPage = 15;

    public function index(){
        $articles = Article::orderBy('created_at', 'desc')->paginate($this->articlePerPage);
        return view('front.blog.index', compact('articles'));
    }

    public function show($slug){
        $article = Article::where('slug', $slug);
        if(!Auth::check() or (Auth::check() && Auth::user()->role != -1)) $article = $article->where('published', true);
        $article = $article->firstOrFail();
        return view('front.blog.show', compact('article'));
    }

    public function postComment(Request $request, $slug){

        if(!Auth::check()){
            $request->validate([
                'usernameComment' => 'required|min:4|alpha_dash|unique:users,name',
                'emailComment' => 'required|email|unique:users,email',
                'websiteComment' => 'url|nullable',
                'comment' => 'required',
                'g-recaptcha-response' => 'required'
            ]);
        }else{
            $request->validate([
                'comment' => 'required',
                'g-recaptcha-response' => 'required'
            ]);
        }

        $comment = new Comment();

        $article = Article::where('slug', $slug);
        if(!Auth::check() or (Auth::check() && Auth::user()->role != -1)) $article = $article->where('published', true);
        $article = $article->firstOrFail();

        $comment->article_id = $article->id;
        if(Auth::check()){
            $comment->user_id = Auth::user()->id;
        }else{
            $comment->username = $request->input('usernameComment');
            $comment->email = $request->input('emailComment');
            $comment->website = $request->input('websiteComment');
        }

        $comment->content = $request->input('comment');
        $comment->ip = $request->ip();
        $comment->save();

        return redirect()->back()->with('commentPublished', true);

    }
}
