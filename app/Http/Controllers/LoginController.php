<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;
use Intervention\Image\Facades\Image;
use App\Mail\forgotPassword;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function index(Request $request){
        $request->validate([
            'name' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('name', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        }else{
            return redirect('/')->withInput()->with('wrong_password', true);
        }
    }

    public function register(){
        return view('front.login.register');
    }

    public function postRegister(Request $request){
        $request->validate([
            'username' => 'required|min:4|alpha_dash|unique:users,name',
            'email' => 'required|email|unique:users',
            'pass1' => 'required|min:6',
            'pass2' => 'required|same:pass1',
            'avatar' => 'image|max:2048|dimensions:min_width:150,min_height:150',
            'g-recaptcha-response' => 'required'
        ]);

        $user = new User();
        $user->name = $request->input('username');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('pass1'));
        $user->role = 0;
        $user->xp = 0;
        $user->remember_token = $request->input('_token');
        if(is_numeric($request->input('fb_id'))) $user->fb_id = $request->input('fb_id');

        if($request->hasFile('avatar')){
            $image = Image::make($request->file('avatar'))->fit(150,150);
            $nameExist = true;
            while($nameExist){
                $avatarName = 'a_'.str_random(rand(6,12)).'.jpg';
                if ( !file_exists( public_path('uploads/avatar/'.$avatarName) ) ) $nameExist = false;
            }
            $image->save('uploads/avatar/'.$avatarName);
            $user->avatar = $avatarName;
        }

        $user->save();

        Auth::login($user, true);
        return redirect('/')->with('success_register', true);
    }

    public function logout(){
        Auth::logout();
        return redirect()->back();
    }

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        // TODO
        return redirect()->route('login.register')->withInput([
                'email' => $user->email,
                'name' => str_slug($user->name),
                'fb_id' => $user->id,
                'avatar' => $user->avatar_original
            ]);


    }

    public function forgot(){
        return view('front.login.forgot');
    }

    public function postForgot(Request $request){
        $request->validate([
            'login' => 'required'
        ]);

        if(filter_var($request->input('login'), FILTER_VALIDATE_EMAIL)){
            $user = User::where('email', $request->input('login'));
        }else{
            $user = User::where('name', $request->input('login'));
        }

        if($user->exists()){
            $user = $user->firstOrFail();
            $passReset = DB::table('password_resets');
            $token = str_random(rand(20,35));
            $data = [
                'email' => $user->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ];
            Mail::to($user->email)->send(new forgotPassword($data));
            if($passReset->where('email', $user->email)->exists()){
                $passReset->where('email', $user->email)->update($data);
            }else{
                $passReset->insert($data);
            }

            return redirect()->back()->with('success_mail_sended', true);
        }else{
            return redirect()->back()->withInput()->with('error_user_not_exists', true);
        }
    }

    public function reset($email, $token){
        if(DB::table('password_resets')->where('email', $email)->where('token', $token)->exists()){
            return view('front.login.reset');
        }else{
            abort(404);
        }
    }

    public function postReset(Request $request, $email, $token){
        if(DB::table('password_resets')->where('email', $email)->where('token', $token)->exists()){
            $request->validate([
                'pass1' => 'required|min:6',
                'pass2' => 'required|same:pass1'
            ]);

            $user = User::where('email', $email)->firstOrFail();
            $user->password = Hash::make($request->input('pass1'));
            $user->save();

            DB::table('password_resets')->where('email', $email)->delete();
            Auth::login($user);
            return redirect('/');
        }else{
            abort(404);
        }
    }
}
