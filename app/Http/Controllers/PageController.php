<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog\Article;

class PageController extends Controller
{
    public function index(){
        $articles = Article::where('published', true)->orderBy('id', 'desc')->take(3)->get();
        return view('front.home', compact('articles'));
    }

    public function index2(){
        $articles = Article::where('published', true)->orderBy('id', 'desc')->take(3)->get();
        return view('front.home2', compact('articles'));
    }

    public function about(){
        return view('front.about');
    }

    public function contact(){
        return view('front.contact');
    }
}
