<?php

namespace App\Http\Controllers;

use App\Models\Industry\Company;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
        $search = $request->input('keywords');
        $keywords = explode(' ', $search);
        if(is_null($search)) $keywords = [];
        if($request->has('keywords')){
            $results = Company::where(function ($query) use ($keywords) {
                foreach ($keywords as $keyword) {
                    $query->orWhere('name', 'like', '%'.$keyword.'%');
                }
            })->paginate(15);
        }else{
            $results = Company::paginate(15);
        }

        return view('front.search.list', compact('results', 'keywords', 'search'));
    }

    public function unsetKeyword($search, $keyword){
        $keywords = explode(' ', $search);
        unset($keywords[$keyword]);
        return redirect()->route('search', ['keywords' => implode(' ', $keywords)]);
    }

    public function jsonCompanies(Request $request){
        if($request->has('query')){
            $data = Company::where("name","LIKE","%{$request->input('query')}%")
                ->get();
        }else{
            $data = Company::all();
        }

        return response()->json($data);
    }
}
