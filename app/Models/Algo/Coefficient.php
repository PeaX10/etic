<?php

namespace App\Models\Algo;

use Illuminate\Database\Eloquent\Model;

class Coefficient extends Model
{
    protected $fillable = ['name', 'value'];
}
