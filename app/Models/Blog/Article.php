<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'description', 'category_id', 'user_id', 'slug'
    ];

    private $imagePath = 'uploads/article';

    public function category(){
        return $this->belongsTo('App\Models\Blog\Category');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function comments(){
        return $this->hasMany('App\Models\Blog\Comment');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Blog\Tag');
    }

    public function getImage(){
        return url($this->imagePath.'/'.$this->image);
    }

}
