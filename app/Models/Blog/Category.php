<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'icon_id'
    ];

    protected $table = 'article_categories';

    public function articles(){
        return $this->hasMany('App\Models\Article');
    }

    public function icon(){
        return $this->belongsTo('App\Models\Icon');
    }
}
