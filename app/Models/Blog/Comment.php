<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $table = 'article_comments';

    protected $fillable = ['username', 'website', 'email', 'content'];

    public function article(){
        return $this->belongsTo('App\Models\Blog\Article');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
