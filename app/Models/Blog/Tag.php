<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'article_tags';

    protected $fillable = ['slug'];

    public function articles(){
        return $this->belongsToMany('App\Models\Blog\Article');
    }

}
