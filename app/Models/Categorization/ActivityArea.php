<?php

namespace App\Models\Categorization;

use Illuminate\Database\Eloquent\Model;

class ActivityArea extends Model
{
    protected $fillable = ['name', 'parent_id'];

    public function parent(){
        return $this->belongsTo('App\Models\Categorization\ActivityArea');
    }

    public function childrens(){
        return $this->hasMany('App\Models\Categorization\ActivityArea', 'parent_id');
    }

    public function getTree(){
        $tree = [];
        $activityArea = $this;
        while($activityArea->parent != null){
            $activityArea = $activityArea->parent;
            $tree[] = $activityArea;
        }
        return array_reverse($tree);
    }

    public function getBigTree(){
        ActivityArea::where('parent_id', null)->get();
    }


}
