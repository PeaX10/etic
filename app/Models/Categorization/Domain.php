<?php

namespace App\Models\Categorization;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    const TOP_DOMAINS = [
        1 => 'Santé',
        2 => 'Ecologie',
        3 => 'Ressources humaines',
        4 => 'Economie',
        5 => 'Production'
    ];

    const DOMAIN_COLORS = [
        1 => 'danger',
        2 => 'success',
        3 => 'info',
        4 => 'warning',
        5 => 'primary'
    ];

    protected $fillable = ['name', 'coefficient', 'parent'];

    public function parent(){
        return self::TOP_DOMAINS[$this->parent];
    }

    public function color(){
        return self::DOMAIN_COLORS[$this->parent];
    }
}
