<?php

namespace App\Models\Industry;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'company_comments';

    protected $fillable = ['username', 'website', 'email', 'content'];

    public function company(){
        return $this->belongsTo('App\Models\Industry\Company');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
