<?php

namespace App\Models\Industry;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Company extends Model
{
    protected $fillable = ['name', 'parent_id', 'good_observation', 'avg_observation', 'bad_observation'];

    private $imagePath = 'uploads/company';

    public function parent(){
        return $this->belongsTo('App\Models\Industry\Company');
    }

    public function getTree(){
        $tree = [];
        $company = $this;
        while($company->parent != null){
            $company = $company->parent;
            $tree[] = $company;
        }
        return array_reverse($tree);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function isHolding() : bool{
        return Company::where('parent_id', $this->id)->exists();
    }

    public function getImage(){
        return url($this->imagePath.'/'.$this->image);
    }

}
