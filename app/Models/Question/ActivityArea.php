<?php

namespace App\Models\Question;

use Illuminate\Database\Eloquent\Model;

class ActivityArea extends Model
{
    protected $table = 'activity_area_questions';

    public function activityArea(){
        return $this->belongsTo(\App\Models\Categorization\ActivityArea::class);
    }
}
