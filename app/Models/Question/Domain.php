<?php

namespace App\Models\Question;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $table = 'domain_questions';

    public function domain(){
        return $this->belongsTo(\App\Models\Categorization\Domain::class);
    }
}
