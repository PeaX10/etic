<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'xp', 'fb_id', 'avatar',
    ];

    private $imagePath = 'uploads/avatar';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles(){
        return $this->hasMany('App\Models\Blog\Article');
    }

    public function isAdmin(){
        if($this->role == -1) return true;
        return false;
    }

    public function getImage(){
        return url($this->imagePath.'/'.$this->avatar);
    }
}
