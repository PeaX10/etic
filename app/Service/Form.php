<?php

namespace App\Service;


class Form{

    private $tree;

    public function generateTree($model, $parentKey = 'parent_id'){
        $this->tree = collect();
        $parent = $model::where($parentKey, null)->get();
        $floor = 0;
        foreach($parent as $k => $v){
            $this->tree->push($v);
            $this->recTree($v, $floor + 1);
        }
        return $this->tree;
    }

    private function recTree($parent, int $floor = 0){
        foreach($parent->childrens as $k => $v){
            $v->name = str_repeat('&nbsp;', $floor*4).'└── '.$v->name;
            $this->tree->push($v);
            $this->recTree($v, $floor + 1);
        }
        return $this->tree;
    }
}
