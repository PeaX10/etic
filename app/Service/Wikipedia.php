<?php

namespace App\Service;


class Wikipedia{

    protected $lang = 'fr';

    protected $url = '';

    protected $data = [];

    const INFOBOX_DELIMITER = [
        'fr' => [
            'start' => "{{Infobox Société\n",
            'end' => "\n}}\n"
        ]
    ];

    public function __construct($lang='fr')
    {
        $this->lang = $lang;
        $this->url = 'http://'. $this->lang.'.wikipedia.org/w/api.php?format=json';
    }

    public function getInfo($object)
    {
        $url = $this->url.'&action=query&titles='.urlencode($object).'&prop=revisions&rvprop=content';
        $this->data = $this->getDataFromUrl($url);
        $pages = $this->data->query->pages;
        $this->data = reset($pages)->revisions[0]->{'*'};
        return $this->data;
    }

    private function getDataFromUrl($url){
        $result = json_decode(file_get_contents($url));
        return $result;
    }

    public function getInfoBox($object){
        $this->getInfo($object);
        //dd($this->data);
        $this->data = $this->getStringBetween($this->data, static::INFOBOX_DELIMITER[$this->lang]['start'], static::INFOBOX_DELIMITER[$this->lang]['end']);
        $this->data = explode("\n", $this->data);
        $this->parseInfoBox();
        return $this->data;
    }

    private function parseInfoBox(){
        $data = [];
        foreach($this->data as $row){
            $row = substr($row, 3);
            $row = explode(' = ', $row);
            if(count($row)== 2) $data[trim($row[0])] = trim($row[1]);
        }
        $this->data = $data;
    }

    private function getStringBetween($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

}
