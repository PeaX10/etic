<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDirectionToQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_area_questions', function (Blueprint $table) {
            $table->smallInteger('direction')->after('coefficient');
        });

        Schema::table('domain_questions', function (Blueprint $table) {
            $table->smallInteger('direction')->after('coefficient');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_area_questions', function (Blueprint $table) {
            $table->removeColumn('direction');
        });

        Schema::table('domain_questions', function (Blueprint $table) {
            $table->removeColumn('direction');
        });

    }
}
