$(window).scroll(function(){
  var $nav = $('nav')
  $nav.toggleClass('scrolled navbar-dark', $(this).scrollTop() > $nav.height());
});