@extends('back.layout.default', ['navLink' => 'algorithme'])

@section('title', 'Dashboard')

@section('css')
    <link rel="stylesheet" href="{{ url('admin_assets/css/extras/roundslider.min.css') }}">
@endsection
@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Paramètrer l'algorithme</span></h4>
        </div>
    </div>
@endsection
@section('content')
    {!! BootForm::open() !!}
    @if(Session::has('adminCoefficientCreated'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> L'algortihme a bien été mis à jour !
        </div>
    @endif
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Domaines</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Santé</h3>
                    <div id="sliderHealth"></div>
                </div>
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Écologie</h3>
                    <div id="sliderEcology"></div>
                </div>
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Ressources humaines</h3>
                    <div id="sliderHumanressource"></div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6" align="center">
                    <h3 class="text-center">Économie</h3>
                    <div id="sliderEconomy"></div>
                </div>
                <div class="col-md-6" align="center">
                    <h3 class="text-center">Production</h3>
                    <div id="sliderProduction"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Poids relatif</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Domaine</h3>
                    <div id="sliderDomain"></div>
                </div>
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Sous-domaine</h3>
                    <div id="sliderSubdomain"></div>
                </div>
                <div class="col-md-4" align="center">
                    <h3 class="text-center">Question</h3>
                    <div id="sliderQuestion"></div>
                </div>
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-lg btn-dark pull-right">Sauvegarder</button>
    {!! BootForm::close() !!}
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/sliders/roundslider.min.js') }}"></script>
    <script type="text/javascript">
        (function($) {

            $.ucfirst = function(str) {

                var text = str;


                var parts = text.split(' '),
                    len = parts.length,
                    i, words = [];
                for (i = 0; i < len; i++) {
                    var part = parts[i];
                    var first = part[0].toUpperCase();
                    var rest = part.substring(1, part.length);
                    var word = first + rest;
                    words.push(word);

                }

                return words.join(' ');
            };

        })(jQuery);

        $( document ).ready(function() {
            var options = {
                radius: 70,
                width: 15,
                max: 100,
                step: 0.1,
                handleSize: "24,12",
                handleShape: "square",
                sliderType: "min-range",
                value:50
            };

            @foreach($coefficients as $coefficient)
                options.value = {{ $coefficient->value }};
                $("#slider{{ ucfirst($coefficient->name) }}").roundSlider(options);
            @endforeach
            @if(count($coefficients) == 0)
            $("#sliderHealth").roundSlider(options);
            $("#sliderEcology").roundSlider(options);
            $("#sliderHumanressource").roundSlider(options);
            $("#sliderEconomy").roundSlider(options);
            $("#sliderProduction").roundSlider(options);

            $("#sliderDomain").roundSlider(options);
            $("#sliderSubdomain").roundSlider(options);
            $("#sliderQuestion").roundSlider(options);
            @endif
        });
    </script>
@endsection
