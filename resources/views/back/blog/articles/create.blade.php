@extends('back.layout.default', ['navLink' => 'blog.articles'])

@section('title', 'Créer un article')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer un article</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer un article de blog</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        {!! BootForm::open(['enctype' => 'multipart/form-data', 'url' => route('admin.blog.articles.store')]) !!}
        <div class="card-body">
            Créer un <code>Article</code> est très simple, il suffit de renseigner tout les champs du formulaire, à savoir qu'il faut tout d'abord créer les <code>tags</code> et les <code>catégories</code> pour pouvoir les liéer à 'article  !
        <hr>
            {!! BootForm::text('title', false, null, ['placeholder' => 'Titre de l\'article', 'maxlength' => 60, 'minlength' => 4]) !!}
            {!! BootForm::text('slug', false, null, ['placeholder' => 'Slug (nom article dans url)']) !!}
            {!! BootForm::textarea('description', false, null, ['placeholder' => 'Description', 'maxlength' => 600, 'minlength' => 50]) !!}
            <div class="form-group ">
                <select class="form-control select-search" data-fouc name="user_id">
                    @foreach($autors as $autor)
                    <option value="{{ $autor->id }}" @if($autor->id == $user->id) selected @endif>{{ $autor->name }} @if($autor->id == $user->id) (moi) @endif</option>
                    @endforeach
                </select>
            </div>
            <div class="form-check form-check-switch form-check-switch-left">
                <label class="form-check-label d-flex align-items-center">
                    <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="En-ligne" data-off-text="Hors-ligne" class="form-check-input-switch" name="published" @if(empty(old('title') and empty(old('published'))) or !empty(old('published') and old('published') == 'on'))) checked @endif>
                </label>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Image de l'article</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            {!! BootForm::file('image', false, ['placeholder' => 'Avatar']) !!}
        </div>
    </div>

    <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Contenu de l'article</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <textarea class="summernote" name="content">
                    @if(empty(old('content')))
                    <h2>Exemple d'article sur Apollo 11</h2>
                    <div class="float-right" style="margin-left: 20px;"><img alt="Saturn V carrying Apollo 11" class="right" src="http://c.cksource.com/a/1/img/sample.jpg"></div>

                    <p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="#">Neil Armstrong</a> and <a href="#">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>

                    <p class="mb-3">Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="#">Michael Collins</a>, piloted the <a href="#">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>

                    <h5 class="font-weight-semibold">Technical details</h5>
                    <p>Launched by a <strong>Saturn V</strong> rocket from <a href="#">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="#">NASA</a>'s Apollo program. The Apollo spacecraft had three parts:</p>
                    <ol>
                        <li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
                        <li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
                        <li><strong>Lunar Module</strong> for landing on the Moon.</li>
                    </ol>
                    <p class="mb-3">After being sent to the Moon by the Saturn V's upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="#">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="#">Pacific Ocean</a> on July 24.</p>

                    <h5 class="font-weight-semibold">Mission crew</h5>

                    <div class="card card-table table-responsive shadow-0">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Position</th>
                                <th>Astronaut</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Commander</td>
                                <td>Neil A. Armstrong</td>
                            </tr>
                            <tr>
                                <td>Command Module Pilot</td>
                                <td>Michael Collins</td>
                            </tr>
                            <tr>
                                <td>Lunar Module Pilot</td>
                                <td>Edwin "Buzz" E. Aldrin, Jr.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a>
                    @else
                    {{ old('content') }}
                    @endif
                </textarea>
            </div>
        </div>

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Tags et catégorie</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form-group ">
                    <select class="form-control select-search" data-fouc name="category_id">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}" data-icon="{{ $category->icon->cssClass }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="tagsinput-tag-objects" data-fouc="" name="tags">
                </div>
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-lg btn-primary">Créer</button>
        </div>
        {!! BootForm::close() !!}
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('.summernote').summernote();
            $('.note-image-input').uniform({
                fileButtonClass: 'action btn bg-warning-400'
            });
            $('.form-check-input-switch').bootstrapSwitch();

            function iformat(icon) {
                var originalOption = icon.element;
                return $('<span><i class="' + $(originalOption).data('icon') + '"></i> ' + icon.text + '</span>');
            }
            $('.select-search').select2({
                width: "100%",
                templateSelection: iformat,
                templateResult: iformat,
                allowHtml: true
            });

            function convertToSlug(Text)
            {
                return Text
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'')
                    ;
            }

            $('input#title').on('input', function() {
               $('input#slug').val(convertToSlug($('input#title').val()));
            });

            // Use Bloodhound engine
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('|'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                limit: 10,
                remote: {
                    url: '{{ route('admin.json.tags') }}'
                }
            });

            // Kicks off the loading/processing of `local` and `prefetch`
            tags.initialize();

            // Define element
            var elt = $('.tagsinput-tag-objects');

            // Initialize
            elt.tagsinput({
                itemValue: 'id',
                itemText: 'slug',
                typeaheadjs: {
                    name: 'tags',
                    displayKey: 'slug',
                    source: tags.ttAdapter()
                }
            });

            @if($errors && !empty(old('tags')))
                @foreach(explode(',', old('tags')) as $tag)
                    elt.tagsinput('add', { "id": "{{ $tag }}" , "slug": "{{ \App\Models\Blog\Tag::find($tag)->slug }}" });
                @endforeach
            @endif

        });
    </script>
@endsection