@extends('back.layout.default', ['navLink' => 'blog.articles'])

@section('title', 'Modifier un article')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier un article</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier un article de blog</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        {!! BootForm::open(['enctype' => 'multipart/form-data', 'url' => route('admin.blog.articles.store').'/'.$article->id, 'method' => 'PUT', 'model' => $article]) !!}
        <div class="card-body">
            Modifier <code>{{ $article->title }}</code> est très simple, il suffit de remplir les champs du formulaire, à savoir qu'il faut tout d'abord créer les <code>tags</code> et les <code>catégories</code> pour pouvoir les liéer à 'article  !
        <hr>
            {!! BootForm::text('title', false, null, ['placeholder' => 'Titre de l\'article', 'maxlength' => 60, 'minlength' => 4]) !!}
            {!! BootForm::text('slug', false, null, ['placeholder' => 'Slug (nom article dans url)']) !!}
            {!! BootForm::textarea('description', false, null, ['placeholder' => 'Description', 'maxlength' => 600, 'minlength' => 50]) !!}
            <div class="form-group ">
                <select class="form-control select-search" data-fouc name="user_id">
                    @if(!empty(old('user_id')))
                        @foreach($autors as $autor)
                            <option value="{{ $autor->id }}" @if(old('user_id') == $user->id) selected @endif>{{ $autor->name }} @if($autor->id == $user->id) (moi) @endif</option>
                        @endforeach
                    @else
                        @foreach($autors as $autor)
                            <option value="{{ $autor->id }}" @if($article->user->id == $user->id) selected @endif>{{ $autor->name }} @if($autor->id == $user->id) (moi) @endif</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-check form-check-switch form-check-switch-left">
                <label class="form-check-label d-flex align-items-center">
                    @if($errors->any())
                        <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="En-ligne" data-off-text="Hors-ligne" class="form-check-input-switch" name="published" @if(!empty(old('published'))) checked @endif>
                    @else
                        <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="En-ligne" data-off-text="Hors-ligne" class="form-check-input-switch" name="published" @if($article->published) checked @endif>
                    @endif
                </label>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Image de l'article</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            @if(!empty($article->image))
                <img src="{{ $article->getImage() }}" class="img-fluid" />
                <hr>
            @endif
            {!! BootForm::file('image', false, ['placeholder' => 'Avatar']) !!}
        </div>
    </div>

    <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Contenu de l'article</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <textarea class="summernote" name="content">
                    @if(empty(old('content')))
                    {{ $article->content }}
                    @else
                    {{ old('content') }}
                    @endif
                </textarea>
            </div>
        </div>

        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Tags et catégorie</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="form-group ">
                    <select class="form-control select-search" data-fouc name="category_id">
                        @if(!empty(old('category_id')))
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if(old('category_id') == $category->id) selected @endif data-icon="{{ $category->icon->cssClass }}">{{ $category->name }}</option>
                            @endforeach
                        @else
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if($article->category->id == $category->id) selected @endif data-icon="{{ $category->icon->cssClass }}">{{ $category->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="tagsinput-tag-objects" data-fouc="" name="tags">
                </div>
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
        </div>
        {!! BootForm::close() !!}
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/editors/summernote/summernote.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/tags/tagsinput.min.js') }}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('.summernote').summernote();
            $('.note-image-input').uniform({
                fileButtonClass: 'action btn bg-warning-400'
            });
            $('.form-check-input-switch').bootstrapSwitch();
            function iformat(icon) {
                var originalOption = icon.element;
                return $('<span><i class="' + $(originalOption).data('icon') + '"></i> ' + icon.text + '</span>');
            }
            $('.select-search').select2({
                width: "100%",
                templateSelection: iformat,
                templateResult: iformat,
                allowHtml: true
            });

            function convertToSlug(Text)
            {
                return Text
                    .toLowerCase()
                    .replace(/ /g,'-')
                    .replace(/[^\w-]+/g,'')
                    ;
            }

            $('input#title').on('input', function() {
                $('input#slug').val(convertToSlug($('input#title').val()));
            });

            // Use Bloodhound engine
            var tags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('|'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                limit: 10,
                remote: {
                    url: '{{ route('admin.json.tags') }}'
                }
            });

            // Kicks off the loading/processing of `local` and `prefetch`
            tags.initialize();

            // Define element
            var elt = $('.tagsinput-tag-objects');

            // Initialize
            elt.tagsinput({
                itemValue: 'id',
                itemText: 'slug',
                typeaheadjs: {
                    name: 'tags',
                    displayKey: 'slug',
                    source: tags.ttAdapter()
                }
            });

            @if($errors && !empty(old('tags')))
                @foreach(explode(',', old('tags')) as $tag)
                    elt.tagsinput('add', { "id": "{{ $tag }}" , "slug": "{{ \App\Models\Blog\Tag::find($tag)->slug }}" });
                @endforeach
            @else
                @foreach($article->tags as $tag)
                elt.tagsinput('add', { "id": "{{ $tag->id }}" , "slug": "{{ $tag->slug }}" });
            @endforeach
            @endif
        });
    </script>
@endsection