@extends('back.layout.default', ['navLink' => 'blog.articles'])

@section('title', 'Articles du blog')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Articles de blog</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="{{ route('admin.blog.articles.create') }}" class="btn bg-danger-400"><i class="fa fa-plus-circle"></i> Créer un article</a>
        </div>
    </div>
@endsection
@section('content')

    @if(Session::has('adminBlogArticleDeletedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Cet article n'existe maintenant plus !
        </div>
    @elseif(Session::has('adminBlogArticleCreatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de créer avec succès un nouvel article
        </div>
    @elseif(Session::has('adminBlogArticleUpdatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de mettre à jour le contenu d'un article
        </div>
    @endif

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Liste des articles</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Les <code>Articles</code> sont affichés ici, en cliquant sur le petit bouton à droite tu pourras directement les gèrer. Pour l'instant les options disponibles sont : <code>créer</code>, <code>prévisualiser</code>, <code>modifier</code>, <code>supprimer</code>, <code>mettre en ligne|hors ligne</code> !
        </div>

        <table class="table datatableBlogArticles">
            <thead>
            <tr>
                <th>Titre</th>
                <th>Slug (url)</th>
                <th>Auteur</th>
                <th>État</th>
                <th>Dernière màj</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articles as $tableArticle)
                <tr>
                    <td>{{ $tableArticle->title }}</td>
                    <td>{{ $tableArticle->slug }}</td>
                    <td>{{ $tableArticle->user->name }}</td>
                    <td>
                        @switch($tableArticle->published)
                            @case(false)
                            <span class="badge badge-danger">Hors-ligne</span>
                            @break

                            @case(true)
                            <span class="badge badge-success">En-ligne</span>
                            @break

                            @default
                            <span class="badge badge-danger">Erreur</span>
                        @endswitch
                    </td>
                    <td>{{ $tableArticle->updated_at->format('d/m/Y') }}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('blog.show', $tableArticle->slug) }}" target="_blank" class="dropdown-item"><i class="icon-eye2"></i> Prévisualiser</a>
                                    <a href="{{ route('admin.blog.articles.show', $tableArticle) }}" class="dropdown-item"><i class="icon-switch"></i> @if($tableArticle->published == false) Mettre en ligne @else Mettre hors-ligne @endif </a>
                                    <a href="{{ route('admin.blog.articles.edit', $tableArticle) }}" class="dropdown-item"><i class="icon-pencil4"></i> Modifier</a>
                                    <a data-toggle="modal" data-target="#modalDeleteBlogArticle" onClick="deleteBlogArticle({{ $tableArticle->id }})" href="javascript:void(0)" class="dropdown-item"><i class="icon-trash"></i> Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div id="modalDeleteBlogArticle" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title"><i class="icon-trash"></i> &nbsp;Supprimer un article</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Attention!</span> Vous ne pourrez pas revenir en arrière.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>

                    <h6 class="font-weight-semibold"><i class="icon-info3 mr-2"></i> Suppresion de l'article</h6>
                    <p>En supprimant cet article, toutes les données relatives à celui-ci seront perdues tels que les <code>photos</code> et <code>commentaires</code>.</p>

                    <hr>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Annuler</button>
                    {!! BootForm::open(['method' => 'DELETE']) !!}
                    <button class="btn bg-danger"><i class="icon-checkmark3 font-size-base mr-1"></i> Supprimer</button>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/pages/datatablesBlogArticles.js') }}"></script>
    <script type="text/javascript">
        function deleteBlogArticle(id){
            $('#modalDeleteBlogArticle form').attr('action', '{{ route('admin.blog.articles.index') }}/'+id);
        }
    </script>
@endsection