@extends('back.layout.default', ['navLink' => 'blog.comments'])

@section('title', 'Modifier un commentaire')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier un commentaire</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier un commentaire</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier un <code>Commenaire</code> est très simple, il suffit de remplir le formulaire !
        <hr>
        {!! BootForm::open(['url' => route('admin.blog.comments.update', $comment), 'method' => 'PUT', 'model' => $comment]) !!}
            @if(empty($comment->user))
                {!! BootForm::text('username', null, null, ['placeholder' => 'Nom d\'utilisateur']) !!}
                {!! BootForm::text('email', null, null, ['placeholder' => 'E-mail']) !!}
                {!! BootForm::text('website', null, null, ['placeholder' => 'Site web']) !!}
            @endif
            {!! BootForm::textarea('content', null, null, ['placeholder' => 'Commentaire']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')

@endsection
