@extends('back.layout.default', ['navLink' => 'blog.tags'])

@section('title', 'Créer un tags')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer un tag</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer un tag</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer un <code>Tag</code> est très simple, il suffit de lui choisir un identifant et le tour est joué !
        <hr>
        {!! BootForm::open(['url' => route('admin.blog.tags.store')]) !!}
            {!! BootForm::text('slug', false, null, ['placeholder' => 'Identifiant du tag (pratique pour être traduit par la suite']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection