@extends('back.layout.default', ['navLink' => 'blog.tags'])

@section('title', 'Modifier un tag')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier un tag</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier un tag</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier un <code>Tag</code> est très simple, il suffit de modifier uniquement son identifiant (slug) !
        <hr>
        {!! BootForm::open(['url' => route('admin.blog.tags.store').'/'.$tag->id, 'method' => 'PUT', 'model' => $tag]) !!}

            {!! BootForm::text('slug', false, null, ['placeholder' => 'Slug (identifiant)']) !!}
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection