@extends('back.layout.default', ['navLink' => 'categorization.activityAreas'])

@section('title', 'Créer un domaine')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer un secteur d'activité @if($parent)<span class="badge badge-dark">{{ $parent->name }}</span>@endif</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer un secteur d'activité</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer un <code>Secteur d'actvité</code> est relativement simple ! Il suffit juste de remplir le formulaire.
        <hr>
        {!! BootForm::open(['url' => route('admin.categorization.activity_areas.store')]) !!}
            {!! BootForm::text('name', false, null, ['placeholder' => 'Nom du secteur d\'activité']) !!}
            @if(!empty($parent))
                {!! BootForm::hidden('parent_id', $parent->id) !!}
            @endif
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')

@endsection