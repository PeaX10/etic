@extends('back.layout.default', ['navLink' => 'categorization.activityAreas'])

@section('title', 'Modifier un secteur d\'activité')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier un secteur d'activité @if($activityArea->parent)<span class="badge badge-dark">{{ $activityArea->parent->name }}</span>@endif</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier un secteur d'activité</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier un <code>Secteur d'actvité</code> est relativement simple ! Il suffit juste de remplir le formulaire.
        <hr>
        {!! BootForm::open(['url' => route('admin.categorization.activity_areas.update', $activityArea), 'method' => 'put', 'model' => $activityArea]) !!}
            {!! BootForm::text('name', false, null, ['placeholder' => 'Nom du secteur d\'activité']) !!}
            {!! BootForm::select('parent_id', 'Parent', \App\Models\Categorization\ActivityArea::pluck('name', 'id')->prepend('', '')) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')

@endsection
