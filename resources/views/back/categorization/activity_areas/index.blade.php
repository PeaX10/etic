@extends('back.layout.default', ['navLink' => 'categorization.activityAreas'])

@section('title', 'Secteur d\'activités')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">
                    @if(!empty($parent))
                        <a href="{{ route('admin.categorization.activity_areas.index') }}">Secteurs d'activités</a>

                        @foreach($parent->getTree() as $breadcum)
                            @if($loop->first) >> @endif
                            <a href="{{ route('admin.categorization.activity_areas.index') }}?parent={{ $breadcum->id }}">{{ $breadcum->name }}</a>
                            @if(!$loop->last) >> @endif
                        @endforeach

                        >> {{ $parent->name }}
                    @else
                        Secteur d'activités
                    @endif
                </span>
            </h4>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="{{ route('admin.categorization.activity_areas.create') }}@if(!empty($parent)){{ '?parent='.$parent->id }}@endif" class="btn bg-danger-400"><i class="fa fa-plus-circle"></i> Créer un secteur d'activité</a>
        </div>
    </div>
@endsection
@section('content')

    @if(Session::has('adminCategorizationActivityAreaDeletedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Ce secteur d'activité n'existe maintenant plus !
        </div>
    @elseif(Session::has('adminCategorizationActivityAreaCreatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de créer avec succès un nouveau secteur d'activité
        </div>
    @elseif(Session::has('adminCategorizationActivityAreaUpdatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de mettre à jour un secteur d'activité
        </div>
    @endif

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Liste des secteurs d'activité</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Les <code>Secteurs d'activités</code> sont affichés ici, en cliquant sur le petit bouton à droite tu pourras directement les gèrer. Pour l'instant les options disponibles sont : <code>créer</code>,  <code>modifier</code>, <code>supprimer</code> !
        </div>

        <table class="table datatableItems">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($activityAreas as $activityArea)
                <tr>
                    <td>{{ $activityArea->id }}</td>
                    <td><a href="{{ route('admin.categorization.activity_areas.index', ['parent' => $activityArea->id]) }}">{{ $activityArea->name }}</a></td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('admin.categorization.activity_areas.edit', $activityArea) }}" class="dropdown-item"><i class="icon-pencil4"></i> Modifier</a>
                                    <a data-toggle="modal" data-target="#modalItem" onClick="deleteItem({{ $activityArea->id }})" href="javascript:void(0)" class="dropdown-item"><i class="icon-trash"></i> Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div id="modalItem" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title"><i class="icon-trash"></i> &nbsp;Supprimer un secteur d'activités</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Attention!</span> Vous ne pourrez pas revenir en arrière.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>

                    <h6 class="font-weight-semibold"><i class="icon-info3 mr-2"></i> Suppresion non réversible</h6>
                    <p>En supprimant cet article, toutes les données relatives à celui-ci seront perdues tels que tous les <code>secteurs d'activités</code> qui lui sont relié.</p>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Annuler</button>
                    {!! BootForm::open(['method' => 'DELETE']) !!}
                    <button class="btn bg-danger"><i class="icon-checkmark3 font-size-base mr-1"></i> Supprimer</button>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/pages/datatablesDefault.js') }}"></script>
    <script type="text/javascript">
        function deleteItem(id){
            $('#modalItem form').attr('action', '{{ route('admin.categorization.activity_areas.index') }}/'+id);
        }
    </script>
@endsection