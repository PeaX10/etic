@extends('back.layout.default', ['navLink' => 'categorization.domains'])

@section('title', 'Créer un domaine')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer un domaine</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer un domaine</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer un <code>Domaine</code> est relativement simple ! Il suffit juste de remplir le formulaire.
        <hr>
        {!! BootForm::open(['url' => route('admin.categorization.domains.store')]) !!}
            {!! BootForm::text('name', false, null, ['placeholder' => 'Nom du domaine']) !!}
            {!! BootForm::text('coefficient', 'Coefficient (en %)', 50, ['class' => 'touchspin-postfix']) !!}
            {!! BootForm::select('parent', 'Domaine', \App\Models\Categorization\Domain::TOP_DOMAINS) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('.touchspin-postfix').TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                postfix: '%'
            });
        });
    </script>
@endsection