@extends('back.layout.default', ['navLink' => 'icons'])

@section('title', 'Modifier une icône')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier une icône</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier une icône</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier une <code>Icône</code> est très simple, il suffit de lui choisir une classe CSS !
        <hr>
        {!! BootForm::open(['url' => route('admin.icons.store').'/'.$icon->id, 'method' => 'PUT', 'model' => $icon]) !!}

            <div class="form-group">
                <div>
                    <span style="font-size: 50px" id="iconPreview" class="{{ $icon->cssClass }}"></span>
                </div>
            </div>
            {!! BootForm::text('cssClass', false, null, ['placeholder' => 'Classe CSS de l\'icône']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#cssClass').on('input', function() {
                $('#iconPreview').attr('class', $('#cssClass').val());
            });
        });
    </script>
@endsection