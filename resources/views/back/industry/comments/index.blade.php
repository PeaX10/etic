@extends('back.layout.default', ['navLink' => 'industry.comments'])

@section('title', 'Commentaires sur les entreprises')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Commentaitres sur les entreprises</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
@endsection
@section('content')

    @if(Session::has('adminIndustryCommentDeletedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Ce commentaire n'existe maintenant plus !
        </div>
    @elseif(Session::has('adminIndustryCommentUpdatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de mettre à jour ce commentaire
        </div>
    @endif

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Liste des commentaires</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Les <code>Commentaires</code> sont affichés ici, en cliquant sur le petit bouton à droite tu pourras directement les gèrer. Pour l'instant les options disponibles sont :  <code>modifier</code> et <code>supprimer</code> !
        </div>

        <table class="table datatableItems">
            <thead>
            <tr>
                <th>Utilisateur</th>
                <th>Entreprise</th>
                <th>Contenu</th>
                <th>Publié le</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $tableComment)
                <tr>
                    <td>
                        @if($tableComment->user)
                            <a href="{{ route('admin.users.edit', $tableComment->user) }}">{{ $tableComment->user->name }}</a>
                        @else
                            {{ $tableComment->username }}<br>{{ $tableComment->email }}<br>{{ $tableComment->website }}
                        @endif
                    </td>
                    <td><a href="{{ route('admin.industry.companies.edit', $tableComment->company) }}">{{ $tableComment->company->name }}</a></td>
                    <td>{{ $tableComment->content }}</td>
                    <td>{{ $tableComment->created_at->format('d/m/Y') }}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('admin.industry.comments.edit', $tableComment) }}" class="dropdown-item"><i class="icon-pencil4"></i> Modifier</a>
                                    <a data-toggle="modal" data-target="#modalDeleteIndustryComment" onClick="deleteIndustryComment({{ $tableComment->id }})" href="javascript:void(0)" class="dropdown-item"><i class="icon-trash"></i> Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div id="modalDeleteIndustryComment" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title"><i class="icon-trash"></i> &nbsp;Supprimer un commentaire</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Attention!</span> Vous ne pourrez pas revenir en arrière.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>

                    <h6 class="font-weight-semibold"><i class="icon-info3 mr-2"></i> Suppresion de la catégorie</h6>
                    <p>En supprimant cet article, toutes les données relatives à celui-ci seront perdues</p>

                    <hr>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Annuler</button>
                    {!! BootForm::open(['method' => 'DELETE']) !!}
                    <button class="btn bg-danger"><i class="icon-checkmark3 font-size-base mr-1"></i> Supprimer</button>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/pages/datatablesDefault.js') }}"></script>
    <script type="text/javascript">
        function deleteIndustryComment(id){
            $('#modalDeleteBlogComment form').attr('action', '{{ route('admin.industry.comments.index') }}/'+id);
        }
    </script>
@endsection
