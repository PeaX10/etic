@extends('back.layout.default', ['navLink' => 'industry.companies'])

@section('title', 'Créer une entreprise')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer une entreprise</span></h4>
        </div>
    </div>
@endsection
@section('content')
    {!! BootForm::open(['enctype' => 'multipart/form-data', 'url' => route('admin.industry.companies.store')]) !!}
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer une entreprise</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer une <code>Entreprise</code> est relativement simple ! Il suffit juste de remplir le formulaire.
        <hr>
            {!! BootForm::text('name', false, null, ['placeholder' => 'Nom de l\'entreprise']) !!}
            <div class="form-group ">
                <select class="form-control select-search" data-fouc name="parent_id">
                    <option value="">Aucune Holding</option>
                    @foreach($companies as $company)
                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                    @endforeach
                </select>
            </div>
            {!! BootForm::textarea('good_observation', false, null, ['placeholder' => 'Bonne observation']) !!}
            {!! BootForm::textarea('avg_observation', false, null, ['placeholder' => 'Observation Moyenne']) !!}
            {!! BootForm::textarea('bad_observation', false, null, ['placeholder' => 'Mauvaise observation']) !!}
        </div>
    </div>

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Image</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            {!! BootForm::file('image', false, ['placeholder' => 'Logo de l\entreprise']) !!}
        </div>
    </div>

    <div class="card infos">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Informations sur l'entreprise</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="tools">
                <a class="btn btn-info text-white" data-toggle="modal" data-target="#wikipediaModal"><i class="fa fa-upload"></i> Charger des informations Wikipédia</a>
                <a class="btn btn-danger text-white" onclick="addInfo()"><i class="icon-add"></i> Ajouter une information</a>
            </div>
            <div class="infoBox" style="margin-top: 25px">

            </div>
        </div>
    </div>

    <div class="card infos">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Actionnaires</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="tools">
                <a class="btn btn-danger text-white" onclick="addShareHolder()"><i class="icon-add"></i> Ajouter un actionnaire</a>
            </div>
            <div class="shareHolderBox" style="margin-top: 25px">

            </div>
        </div>
    </div>

    <div class="text-right">
        <button type="submit" class="btn btn-lg btn-primary">Créer</button>
    </div>
    {!! BootForm::close() !!}
    <div class="modal fade" id="wikipediaModal" tabindex="-1" role="dialog" aria-labelledby="wikipediaModalTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="wikipediaModal">Charger des informations Wikipédia</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group ">
                        <div>
                            <input placeholder="Titre de la page" class="form-control" id="wikipedia" name="wikipedia" type="text">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="button" class="btn btn-primary" id="loadData">Charger</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js') }}"></script>
    <script type="text/javascript">

        var cptInfo = 0;
        var cptShareHolder = 0;

        function deleteInfo(id){
          $('#info'+id).parent().parent().remove();
        }

        function addInfo(name = "", value=""){
          cptInfo++;
          $('.infoBox').append(
            '<div class="form-group row">' +
            '<div class="col-sm-11">' +
            '<label><input type="text" name="infoNames[]" class="form-control" placeholder="Nom de l\'information" value="'+ name +'"></label>' +
            '<textarea cols="50" rows="10" class="form-control" name="infoDatas[]" id="info'+ cptInfo +'">' + value + '</textarea>' +
            '</div>' +
            '<div class="col-md-1 text-danger" style="position: relative"><span onclick="deleteInfo('+ cptInfo +')" style=" position: absolute;top: 50%;transform: translateY(-50%); cursor: pointer"><i class="fa fa-trash"></i></span></div>'+
            '</div>'
          );
        }

        function deleteShareHolder(id){
          $('#shareHolder'+id).parent().parent().remove();
        }

        function addShareHolder(value=""){
          cptShareHolder++;
          $('.shareHolderBox').append(
            '<div class="form-group row">' +
            '<div class="col-sm-11">' +
            '<input placeholder="Entrer le nom d\'un actionnaire" class="form-control" name="shareHolders[]" id="shareHolder'+ cptShareHolder +'" value="' + value + '">' +
            '</div>' +
            '<div class="col-md-1 text-danger" style="position: relative"><span onclick="deleteShareHolder('+ cptShareHolder +')" style=" position: absolute;top: 50%;transform: translateY(-50%); cursor: pointer"><i class="fa fa-trash"></i></span></div>'+
            '</div>'
          );
        }

        $( document ).ready(function() {

            $('.select-search').select2({
                width: "100%",
            });

            $('#loadData').click(function(){
                $.ajax({
                    url: "{{  url('admin/json/wikipedia') }}/"+$('#wikipedia').val(),
                    context: document.body
                }).done(function(data) {
                    data = $.parseJSON(data);
                    $.each(data, function( index, value ) {
                        addInfo(index, value);
                    });
                    $('#wikipediaModal').modal('hide');
                }).fail(function(data){
                    alert('Cette page wikipédia n\est pas accessible !')
                });
            });

            @if(!empty(old('infoDatas')))
                @foreach(old('infoDatas') as $k => $infoData)
                    addInfo("{{ old('infoNames')[$k] }}", "{{ $infoData }}");
                @endforeach
            @endif

            @if(!empty(old('shareHolders')))
                @foreach(old('shareHolders') as $shareHolder)
                    addShareHolder("{{ $shareHolder }}");
                @endforeach
            @endif
        });
    </script>

@endsection
