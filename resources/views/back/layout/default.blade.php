<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin Etic - @yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('admin_assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    @yield('css')


    <script src="{{ url('admin_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ url('admin_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('admin_assets/js/plugins/loaders/blockui.min.js') }}"></script>

    <script src="{{ url('admin_assets/js/app.js') }}"></script>
    <script src="{{ url('admin_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ url('admin_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    @yield('js')
</head>

<body class="navbar-top">

@include('back.layout.menu')

<div class="page-content">

    @include('back.layout.sidebar')

    <div class="content-wrapper">
        <div class="page-header">
            @yield('header')
        </div>

        <div class="content pt-0">

           @yield('content')

        </div>

        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2018. Etic.io
					</span>
            </div>
        </div>
    </div>
</div>
</body>
</html>
