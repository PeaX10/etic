<div class="navbar navbar-expand-md navbar-light fixed-top">

    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <div class="navbar-brand navbar-brand-md">
            <a href="{{ route('admin.index') }}" class="d-inline-block">
                <img src="{{ url('admin_assets/images/logo_light.png') }}" alt="">
            </a>
        </div>

        <div class="navbar-brand navbar-brand-xs">
            <a href="{{ route('admin.index') }}" class="d-inline-block">
                <img src="{{ url('admin_assets/images/logo_icon_light.png') }}" alt="">
            </a>
        </div>
    </div>

    <div class="d-flex flex-1 d-md-none">
        <div class="navbar-brand mr-auto">
            <a href="{{ route('admin.index')  }}" class="d-inline-block">
                <img src="{{ url('admin_assets/images/logo_dark.png') }}" alt="">
            </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>

        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>
        <span class="navbar-text ml-md-3 mr-md-auto">
				<a href="{{ route('home') }}" class="badge bg-green-600 badge-pill">Aller sur le site</a>
        </span>


        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link">
                    <i class="icon-bell2"></i>
                    <span class="d-md-none ml-2">Notifications</span>
                    <span class="badge badge-mark border-white"></span>
                </a>
            </li>

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ url('admin_assets/images/image.png') }}" class="rounded-circle" alt="">
                    <span>{{ $user->name }}</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{ route('admin.users.edit', Auth::user()) }}" class="dropdown-item"><i class="icon-user"></i> Mon compte</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Déconnexion</a>
                </div>
            </li>
        </ul>
    </div>

</div>