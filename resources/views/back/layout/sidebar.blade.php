<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">


    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>

    <div class="sidebar-content">

        <div class="sidebar-user">
            <div class="card-body">
                <div class="media">
                    <div class="mr-3">
                        <a href="#"><img src="{{ url('admin_assets/images/image.png') }}" width="38" height="38" class="rounded-circle" alt=""></a>
                    </div>

                    <div class="media-body">
                        <div class="media-title font-weight-semibold">{{ $user->name }}</div>
                        <div class="font-size-xs opacity-50">
                            <i class="icon-medal2 font-size-sm"></i> &nbsp;Administrateur
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Principal</div> <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link @if($navLink == 'dashboard') active @endif">
                        <i class="icon-home4"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="icon-statistics"></i> <span>Statistiques</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.users.index') }}" class="nav-link @if($navLink == 'users') active @endif">
                        <i class="icon-users"></i>
                        <span>Utilisateurs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="fa fa-calendar-check-o"></i>
                        <span>Question à vérifier</span>
                        <span class="badge bg-danger-400 align-self-center ml-auto">15</span>
                    </a>
                </li>
                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Contenu</div> <i class="icon-menu" title="Contenu"></i></li>
                <li class="nav-item nav-item-submenu @if(in_array($navLink, ['blog.articles', 'blog.categories', 'blog.tags', 'blog.comments'])) nav-item-expanded nav-item-open @endif">
                    <a href="#" class="nav-link"><i class="icon-newspaper"></i> <span>Blog</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Blog">
                        <li class="nav-item"><a href="{{ route('admin.blog.articles.index') }}" class="nav-link @if($navLink == 'blog.articles') active @endif">Articles</a></li>
                        <li class="nav-item"><a href=" {{  route('admin.blog.categories.index') }}" class="nav-link @if($navLink == 'blog.categories') active @endif">Catégories</a></li>
                        <li class="nav-item"><a href="{{ route('admin.blog.tags.index') }}" class="nav-link @if($navLink == 'blog.tags') active @endif">Tags</a></li>
                        <li class="nav-item"><a href="{{ route('admin.blog.comments.index') }}" class="nav-link @if($navLink == 'blog.comments') active @endif">Commentaires</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu @if(in_array($navLink, ['categorization.domains', 'categorization.activityAreas'])) nav-item-expanded nav-item-open @endif">
                    <a href="#" class="nav-link"><i class="fa fa-sitemap"></i> <span>Catégorisation</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Catégorisation">
                        <li class="nav-item"><a href="{{ route('admin.categorization.domains.index') }}" class="nav-link @if($navLink == 'categorization.domains') active @endif">Domaines</a></li>
                        <li class="nav-item"><a href="{{ route('admin.categorization.activity_areas.index') }}" class="nav-link @if($navLink == 'categorization.activityAreas') active @endif">Secteur d'activité</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu @if(in_array($navLink, ['industry.companies', 'industry.comments'])) nav-item-expanded nav-item-open @endif">
                    <a href="#" class="nav-link"><i class="fa fa-industry"></i> <span>Industrie</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Entreprises">
                        <li class="nav-item"><a href="{{ route('admin.industry.companies.index') }}" class="nav-link @if($navLink == 'industry.companies') active @endif">Entreprises</a></li>
                        <li class="nav-item"><a href="{{ route('admin.industry.comments.index') }}" class="nav-link @if($navLink == 'industry.comments') active @endif">Commentaires</a></li>
                    </ul>
                </li>

                <li class="nav-item nav-item-submenu @if(in_array($navLink, ['question.domains', 'question.activityAreas'])) nav-item-expanded nav-item-open @endif">
                    <a href="#" class="nav-link"><i class="fa fa-question"></i> <span>Questions</span></a>
                    <ul class="nav nav-group-sub" data-submenu-title="Questions">
                        <li class="nav-item"><a href="{{ route('admin.question.domains.index') }}" class="nav-link @if($navLink == 'question.domains') active @endif">Domaines</a></li>
                        <li class="nav-item"><a href="{{ route('admin.question.activity_areas.index') }}" class="nav-link @if($navLink == 'question.activityAreas') active @endif">Secteur d'activités</a></li>
                    </ul>
                </li>

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Global</div> <i class="icon-menu" title="Global"></i></li>
                <li class="nav-item">
                    <a href="{{ route('admin.algorithme') }}" class="nav-link @if($navLink == 'algorithme') active @endif"><i class="icon-atom2"></i> <span>Paramètrer l'algorithme</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.icons.index') }}" class="nav-link @if($navLink == 'icons') active @endif">
                        <i class="icon-design"></i> <span>Icônes</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="icon-gear"></i> <span>Paramètres</span>
                    </a>
                </li>

            </ul>
        </div>
    </div>


</div>
