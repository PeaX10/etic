@extends('back.layout.default', ['navLink' => 'question.activityAreas'])

@section('title', 'Créer une question aux secteurs d\'activités')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Créer une question de secteur d'activité</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer une question de secteur d'activité</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Créer une <code>question</code> est très simple, il suffit de d'entrér la question et indiquer un coefficient !
        <hr>
        {!! BootForm::open(['url' => route('admin.question.activity_areas.store')]) !!}
            <div class="form-group ">
                <label for="activityArea" class="control-label">Secteur d'activité</label>
                <select class="form-control select-search" data-fouc name="activityArea">
                    @foreach($activityAreas as $aA)
                        <option value="{{ $aA->id }}" @if(old('activityArea') == $aA->id) selected @endif>{!! $aA->name !!}</option>
                    @endforeach
                </select>
            </div>
            {!! BootForm::textarea('question', false, null, ['placeholder' => 'Entrer une question']) !!}
            {!! BootForm::text('coefficient', 'Coefficient (en %)', 50, ['class' => 'touchspin-postfix']) !!}
            <div class="form-check form-check-switch form-check-switch-left">
                <label class="form-check-label d-flex align-items-center">
                    <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Positif" data-off-text="Négatif" class="form-check-input-switch" name="direction" @if(empty(old('title') and empty(old('published'))) or !empty(old('published') and old('published') == 'on'))) checked @endif>
                </label>
            </div>
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Créer</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        $('.select-search').select2();
        $('.touchspin-postfix').TouchSpin({
          min: 0,
          max: 100,
          step: 0.1,
          decimals: 2,
          postfix: '%'
        });
      });
    </script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        $('.form-check-input-switch').bootstrapSwitch();
      });
    </script>
@endsection