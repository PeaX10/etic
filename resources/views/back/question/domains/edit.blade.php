@extends('back.layout.default', ['navLink' => 'question.domains'])

@section('title', 'Modifier une question de domaine')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier une question de domaine</span></h4>
        </div>
    </div>
@endsection
@section('content')

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Créer une question de domaine</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier une <code>question</code> est très simple, il suffit de d'entrér la question et indiquer un coefficient !
            <hr>
            {!! BootForm::open(['url' => route('admin.question.domains.update', $domain), 'method' => 'PUT', 'model' => $domain]) !!}
            {!! BootForm::select('domain', 'Domaine', $domains) !!}
            <a class="btn btn-sm btn-outline-primary" target="_blank" href="{{ route('admin.categorization.domains.create') }}">Créer un domaine</a>
            <hr>
            {!! BootForm::textarea('question', false, null, ['placeholder' => 'Entrer une question']) !!}
            {!! BootForm::text('coefficient', 'Coefficient (en %)', null, ['class' => 'touchspin-postfix']) !!}
            <div class="form-check form-check-switch form-check-switch-left">
                <label class="form-check-label d-flex align-items-center">
                    @if($errors->any())
                        <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Positive" data-off-text="Négative" class="form-check-input-switch" name="direction" @if(!empty(old('direction'))) checked @endif>
                    @else
                        <input type="checkbox" data-on-color="success" data-off-color="danger" data-on-text="Positive" data-off-text="Négative" class="form-check-input-switch" name="direction" @if($domain->direction == 1) checked @endif>
                    @endif
                </label>
            </div>
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        $('#cssClass').on('input', function() {
          $('#iconPreview').attr('class', $('#cssClass').val());
        });
        $('.touchspin-postfix').TouchSpin({
          min: 0,
          max: 100,
          step: 0.1,
          decimals: 2,
          postfix: '%'
        });
      });
    </script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        $('.form-check-input-switch').bootstrapSwitch();
      });
    </script>
@endsection