@extends('back.layout.default', ['navLink' => 'question.domains'])

@section('title', 'Questions relatives aux différent domaines')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Questions relatives aux différents domaines</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="{{ route('admin.question.domains.create') }}" class="btn bg-danger-400"><i class="fa fa-plus-circle"></i> Créer une question</a>
        </div>
    </div>
@endsection
@section('content')

    @if(Session::has('adminQuestionDomainDeletedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Cette question n'existe maintenant plus !
        </div>
    @elseif(Session::has('adminQuestionDomainUpdatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de mettre à jour cette question
        </div>
    @endif

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Liste des questions</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Les <code>Questions</code> sont affichés ici, en cliquant sur le petit bouton à droite tu pourras directement les gèrer. Pour l'instant les options disponibles sont :  <code>ajouter</code>, <code>modifier</code> et <code>supprimer</code> !
            <br>
            <br>
            @foreach(\App\Models\Categorization\Domain::TOP_DOMAINS as $key => $domain)
                <button class="btn btn-sm btn-{{ \App\Models\Categorization\Domain::DOMAIN_COLORS[$key] }}">{{ $domain }}</button>
            @endforeach
        </div>

        <table class="table datatableItems">
            <thead>
            <tr>
                <th>Question</th>
                <th>Coefficient</th>
                <th>Sens</th>
                <th>Domaine</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($questions as $tableQuestion)
                <tr>
                    <td class="w-75">{{ $tableQuestion->question }}</td>
                    <td>{{ $tableQuestion->coefficient }}%</td>
                    <td>
                        @if($tableQuestion->direction === 1)
                            <button class="btn btn-sm btn-success">Positif</button>
                        @elseif($tableQuestion->direction === -1)
                            <button class="btn btn-sm btn-danger">Négatif</button>
                        @else
                            <button class="btn btn-sm btn-secondary">?</button>
                        @endif
                    </td>
                    <td><a href="{{ route('admin.categorization.domains.edit', $tableQuestion->domain) }}" class="btn btn-xs btn-{{ $tableQuestion->domain->color() }}">{{ $tableQuestion->domain->name }}</a></td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('admin.question.domains.edit', $tableQuestion) }}" class="dropdown-item"><i class="icon-pencil4"></i> Modifier</a>
                                    <a data-toggle="modal" data-target="#modalDeleteDomainQuestion" onClick="deleteDomainQuestion({{ $tableQuestion->id }})" href="javascript:void(0)" class="dropdown-item"><i class="icon-trash"></i> Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div id="modalDeleteDomainQuestion" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title"><i class="icon-trash"></i> &nbsp;Supprimer une question</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Attention!</span> Vous ne pourrez pas revenir en arrière.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>

                    <h6 class="font-weight-semibold"><i class="icon-info3 mr-2"></i> Suppresion de la question</h6>
                    <p>En supprimant cette question, toutes les données relatives à celle-ci seront perdues</p>

                    <hr>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Annuler</button>
                    {!! BootForm::open(['method' => 'DELETE']) !!}
                    <button class="btn bg-danger"><i class="icon-checkmark3 font-size-base mr-1"></i> Supprimer</button>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/pages/datatablesDefault.js') }}"></script>
    <script type="text/javascript">
        function deleteDomainQuestion(id){
            $('#modalDeleteDomainQuestion form').attr('action', '{{ route('admin.question.domains.index') }}/'+id);
        }
    </script>
@endsection
