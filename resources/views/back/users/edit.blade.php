@extends('back.layout.default', ['navLink' => 'users'])

@section('title', 'Modifier un utilisateur')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Modifier un utilisateur</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
@endsection
@section('content')


    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Modifier l'utilisateur: {{ $eUser->name }}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Modifier <code>{{ $eUser->name }}</code> est très simple, il suffit de renseigner tout les champs du formulaire ci-dessous. Pour changer d'avatar il suffit simplement de choisir une photo avec le champs du formulaire en question !
            Si vous ne souhaitez pas modifier le mot de passe ou l'avatar, il suffit de laisser les champs vides !
        <hr>
        {!! BootForm::open(['enctype' => 'multipart/form-data', 'url' => route('admin.users.update', $eUser), 'model' => $eUser, 'method' => 'PUT']) !!}
            {!! BootForm::text('name', 'Nom d\'utilisateur', null, ['placeholder' => 'Nom d\'utilisateur']) !!}
            {!! BootForm::email('email', 'Email', null, ['placeholder' => 'E-mail']) !!}
            {!! BootForm::number('xp', 'Nombre d\'XP', null, ['placeholder' => 'Nombre de point d\'XP (numérique)', 'min' => 0]) !!}
            {!! BootForm::select('role', 'Rôle', [0 => 'Utilisateur', -1 => 'Administrateur']) !!}
            {!! BootForm::text('fb_id', 'Facebook ID', null, ['placeholder' => 'ID Facebook']) !!}
            <hr>
            {!! BootForm::file('avatar', 'Avatar', ['placeholder' => 'Avatar']) !!}
            {!! BootForm::password('pass1', false, ['placeholder' => 'Mot de passe']) !!}
            {!! BootForm::password('pass2', false, ['placeholder' => 'Confirmer le mot de passe']) !!}
            <hr>
            <div class="text-right">
                <button type="submit" class="btn btn-lg btn-primary">Modifier</button>
            </div>
        {!! BootForm::close() !!}
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript">
        $('.form-control-uniform').uniform();
    </script>
@endsection