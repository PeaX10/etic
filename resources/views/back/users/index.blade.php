@extends('back.layout.default', ['navLink' => 'users'])

@section('title', 'Utilisateurs')

@section('header')
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4><span class="font-weight-semibold">Utilisateurs</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none text-center text-md-left mb-3 mb-md-0">
            <a href="{{ route('admin.users.create') }}" class="btn bg-danger-400"><i class="fa fa-plus-circle"></i> Créer un utilisateur</a>
        </div>
    </div>
@endsection
@section('content')

    @if(Session::has('adminUserDeletedError'))
        <div class="alert bg-danger text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Oups!</span> Vous ne pouvez pas supprimer votre propre compte <a href="#" class="alert-link">Réessayer avec un autre compte</a>.
        </div>
    @elseif(Session::has('adminUserDeletedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Ce méchant utilisateur a maintenant été supprimé
        </div>
    @elseif(Session::has('adminUserCreatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de créer avec succès un nouveau utilisateur
        </div>
    @elseif(Session::has('adminUserUpdatedSuccess'))
        <div class="alert bg-success text-white alert-styled-right alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <span class="font-weight-semibold">Super!</span> Vous venez de mettre à jour le profil d'un utilisateur
        </div>
    @endif

    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Liste des utilisateurs</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            Les <code>Utilisateurs</code> sont affichés ici, en cliquant sur le petit bouton à droite tu pourras directement les gèrer. Pour l'instant les options disponibles sont : <code>créer</code>, <code>se connecter</code>, <code>modifier</code>, <code>supprimer</code> !
        </div>

        <table class="table datatableUsers">
            <thead>
            <tr>
                <th>Nom</th>
                <th>E-mail</th>
                <th>FB</th>
                <th>XP</th>
                <th>Role</th>
                <th>Inscrit le</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $tableUser)
                <tr>
                    <td>{{ $tableUser->name }}</td>
                    <td><a href="mailto:{{ $tableUser->email }}">{{ $tableUser->email }}</a></td>
                    <td>{{ $tableUser->fb_id }}</td>
                    <td>{{ $tableUser->xp }}</td>
                    <td>
                        @switch($tableUser->role)
                            @case(-1)
                            <span class="badge badge-dark">Administrateur</span>
                            @break

                            @case(0)
                            <span class="badge badge-primary">Utilisateur</span>
                            @break

                            @default
                            <span class="badge badge-warning">Non défini</span>
                        @endswitch
                    </td>
                    <td>{{ $tableUser->created_at->format('d/m/Y') }}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('admin.users.show', $tableUser) }}" class="dropdown-item"><i class="icon-key"></i> Se connecter</a>
                                    <a href="{{ route('admin.users.edit', $tableUser) }}" class="dropdown-item"><i class="icon-pencil4"></i> Modifier</a>
                                    <a data-toggle="modal" data-target="#modalDeleteUser" onClick="deleteUser({{ $tableUser->id }})" href="javascript:void(0)" class="dropdown-item"><i class="icon-trash"></i> Supprimer</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div id="modalDeleteUser" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title"><i class="icon-trash"></i> &nbsp;Supprimer un utilisateur</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible alert-styled-left border-top-0 border-bottom-0 border-right-0">
                        <span class="font-weight-semibold">Attention!</span> Vous ne pourrez pas revenir en arrière.
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>

                    <h6 class="font-weight-semibold"><i class="icon-info3 mr-2"></i> Suppresion de l'utilisateur</h6>
                    <p>Toutes les données associées à ce compte seront supprimées tels que : ses photos, ses commentaires ainsi que toutes autres participations.</p>

                    <hr>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross2 font-size-base mr-1"></i> Annuler</button>
                    {!! BootForm::open(['method' => 'DELETE']) !!}
                        <button class="btn bg-danger"><i class="icon-checkmark3 font-size-base mr-1"></i> Supprimer</button>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{ url('admin_assets/js/pages/datatablesUsers.js') }}"></script>
    <script type="text/javascript">
        function deleteUser(id){
            $('#modalDeleteUser form').attr('action', '{{ route('admin.users.index') }}/'+id);
        }
    </script>
@endsection