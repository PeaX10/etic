<!doctype html>
<html lang="{{ Config::get('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/email/style.css') }}">
</head>
<body>
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            @yield('content')
        </div>
    </div>
</body>
</html>