@extends('email.layout.default')

@section('content')
    <h1>Voici un lien pour réinitialiser ton mot de passe il est valide 24h</h1>
    <br>
    <p>Merci de cliquer sur ce lien pour réinitialiser ton mot de passe : </p>
    <a class="btn btn-block btn-success" href="{{ url('login/reset/'.$data['email'].'/'.$data['token']) }}">Réinitailiser mon mot de passe</a>
    <br>
    <p>>Si vous n'arrivez pas à voir le bouton merci de cliquer sur le lien suivant:</p>
    <a href="{{ url('login/reset/'.$data['email'].'/'.$data['token']) }}">{{ url('login/reset/'.$data['email'].'/'.$data['token']) }}</a>
@endsection