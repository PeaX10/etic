@extends('front.layout.default', ['menu' => 'blog', 'bodyClass' => 'gl-blog-page gl-blog-grid'])

@section('title', 'Blog')

@section('css')
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection

@section('content')
    <section class="gl-page-content-section">
        <div class="container">
            <div class="row">
                <h1>Les derniers articles sur Etic</h1>
                <hr class="pb-5">
                <div class="gl-blog-content gl-blog-grid-wrapper">

                    @foreach($articles as $article)
                    <div class="gl-blog-items gl-image-post col-md-4 col-sm-4 col-xs-12">
                        <div class="gl-blog-img-wrapper">
                            <img src="{{ $article->getImage() }}" alt="Image - {{ $article->title }}">
                            <div class="gl-blog-cat-icon">
                                <i class="{{ $article->category->icon->cssClass }}"></i>
                            </div>
                        </div>

                        <div class="gl-blog-item-details">
                            <h3>
                                <a href="{{ url('blog/'.$article->slug) }}">{{ $article->title }}</a>
                            </h3>
                            <p>{{ $article->description }}</p>
                            <span class="gl-blog-post-date">{{ \Carbon\Carbon::make($article->created_at)->format('d M Y') }}</span>
                            <span class="gl-blog-author pull-right">{{ $article->user->name }}</span>
                        </div>
                    </div>
                    @endforeach
                    @if(count($articles) == 0)
                        <p>Oups il n'y pas d'articles pour le moment...</p>
                    @endif
                </div>

                <div class="container text-center">
                    {{ $articles->links() }}
                </div>
            </div>
        </div>
    </section>

@endsection