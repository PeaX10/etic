@extends('front.layout.default', ['menu' => 'contact', 'bodyClass' => 'gl-contact-us-template'])

@section('title', 'Contactez-nous')

@section('css')
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection

@section('content')
    <section class="gl-page-content-section">
        <div class="container">
            <div class="gl-row">
                <h2 class="gl-sub-heading">Contactez-nous</h2>

                {!! BootForm::open(['class' => 'gl-contact-form', 'autocomplete' => 'off']) !!}
                    <input type="text" name="nale" id="name" placeholder="NOM Prénom">
                    <input type="email" name="email" id="email" placeholder="E-mail">
                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                    <input type="submit" value="Send" class="gl-btn">
                {!! BootForm::close() !!}
            </div>
        </div>
    </section>

@endsection