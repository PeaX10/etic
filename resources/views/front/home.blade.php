@extends('front.layout.default')

@section('title', 'Accueil')

@section('css')
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection

@section('content')
<section class="gl-hero-img-wrapper">
    <div class="container">
        <div class="row">
            <div class="gl-elements-content-wrapper">
                <div id="typed-strings">
                    <p>La note <span class="gl-color-text">"etic"</span> des entreprises</p>
                </div>
                <h2 id="gl-slogan" class="gl-hero-text-heading">Changeons le <span class="gl-color-text">monde</span> par notre consommation</h2>
                <p class="gl-hero-text-paragraph">Changeons le <span class="gl-color-text">monde</span> par notre consommation</p>

                <div class="gl-directory-searchbar gl-bz-directory-searchbar">
                    <form action="{{ route('search') }}" id="gl-bz-directory-form">
                        <fieldset>
                            <input type="text" name="keywords" id="gl-business-keyword" class="gl-directory-input" placeholder="Mots clés..." autocomplete="off">
                        </fieldset>

                        <button type="submit" class="gl-icon-btn"><i class="fa fa-search"></i> Rechercher</button>
                    </form>
                </div>

                <div class="gl-scroll-down-wrapper">
                    <a href="#gl-next-section" class="gl-scroll-down"><i class="ion-chevron-down"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gl-service-section-wrapper gl-section-wrapper" id="gl-next-section">
    <div class="container">
        <div class="row">
            <div class="gl-service-we-offer">

                <div class="gl-icon-top-with-text col-md-4 col-sm-4 col-xs-12 appear fadeIn" data-wow-duration="1s" data-wow-delay=".7s">
                    <div class="gl-icon-wrapper">
                        <i class="fas fa-heart"></i>
                    </div>
                    <h3>Fait avec amour</h3>
                    <p>Mettre du texte ici, j'ai pas trop d'idée</p>
                </div>

                <div class="gl-icon-top-with-text col-md-4 col-sm-4 col-xs-12 appear fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="gl-icon-wrapper">
                        <i class="fas fa-info"></i>
                    </div>
                    <h3>Informations vérifiées</h3>
                    <p>Mettre du texte ici, j'ai pas trop d'idée</p>
                </div>

                <div class="gl-icon-top-with-text col-md-4 col-sm-4 col-xs-12 appear fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="gl-icon-wrapper">
                        <i class="fas fa-leaf"></i>
                    </div>
                    <h3>Engager pour la nature</h3>
                    <p>Mettre du texte ici, j'ai pas trop d'idée</p>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="gl-what-you-need-section gl-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="gl-section-headings">
                <h1>ETIC qu'est ce que c'est ?</h1>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/evRxgO8hye4" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="gl-feat-listing-section gl-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="gl-section-headings">
                <h1>Les derniers articles</h1>
                <p>Des articles écrit avec soin pour votre plus grand plaisir</p>
            </div>
            <div class="gl-row">
            @foreach ($articles as $article)
                <div class="gl-blog-items gl-image-post col-md-4 col-sm-4 col-xs-12">
                    <div class="gl-blog-img-wrapper">
                        <picture>
                            <img alt="Category Image" src="{{ $article->getImage() }}">
                        </picture>
                        <div class="gl-blog-cat-icon">
                            <i class="{{ $article->category->icon->cssClass }}"></i>
                        </div>
                    </div>

                    <div class="gl-blog-item-details">
                        <h3>
                            <a href="{{ url('blog/'.$article->slug) }}">{{ $article->title }}</a>
                        </h3>
                        <p>{{ $article->description }}</p>
                        <span class="gl-blog-post-date">{{ \Carbon\Carbon::make($article->created_at)->format('d M Y') }}</span>
                        <span class="gl-blog-author pull-right">{{ $article->user->name }}</span>
                    </div>
                </div>
            @endforeach
            </div>
            <div class="gl-more-btn-wrapper">
                <a href="{{ url('blog') }}" class="gl-more-btn gl-btn">Plus d'articles</a>
            </div>
        </div>
    </div>
</section>

<section class="gl-how-it-works-section gl-custom-section gl-section-wrapper">
    <div class="container">
        <div class="row">
            <div class="gl-custom-sec-content">
                <h1>Comment ça marche ?</h1>
                <p>Etic s'enrichie tous les jours avec des membres actifs qui nous soumettent des informations que notre équipe va vérifier avec soin afin de vous fournir des informations de qualité.</p>

                <a href="#" class="gl-btn gl-more-btn">En savoir plus</a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="gl-custom-image-wrapper">
                <img src="images/custom-img-1.jpg" alt="Custom Image" class="gl-lazy">
            </div>
        </div>
    </div>
</section>

<section class="gl-cta-section">
    <div class="container">
        <div class="row">
            <div class="gl-cta-content-wrapper">
                <p> Découvre notre application mobile dès le mois de <span class="gl-bold-font">janvier</span> sur votre store </p>
                <ul>
                    <li>
                        <a href="#" class="gl-google-play-btn"></a>
                    </li>
                    <li>
                        <a href="#" class="gl-app-store-btn"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
    <script src="{{ url('js/typeahead.min.js') }}"></script>
    <script type="text/javascript">
      var companies = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: '{{ route('search.companies.json') }}?query=%QUERY',
          wildcard: '%QUERY'
        }
      });

      $('#gl-business-keyword').typeahead(null, {
        name: 'companies',
        display: 'name',
        source: companies,
        templates: {
          suggestion: function(data){
            return '<p style="line-height: 40px"><img src="uploads/company/' + data.image + '" alt="Logo ' + data.name + '" style="max-width: 40px"> <strong style="padding-left:20px;">' + data.name + '</strong></p>';
          }
        }
      });
    </script>
@endsection