@extends('front.layout.new.layout')

@section('title', 'Accueil')

@section('css')

@endsection

@section('content')

    <section class="py-5" id="search">
        <div class="container pt-5">
            <div class="title text-center">
                <h1>La note <strong class="text-red">"etic"</strong> des entreprises</h1>
                <h3><small>Changons le monde par notre consomation</small></h3>
            </div>
            <div class="input-group search mt-5">
                <input type="text" class="form-control" placeholder="Rechercher une entreprise..." name="search">
                <div class="input-group-append">
                    <button class="btn btn-success bg-green px-4" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-3">
        <div class="container py-5">
            <h3 class="">Les entreprises les mieux noté</h3>
            <div class="row pt-3">
                <div class="col">
                    <div class="card">
                        <img class="card-img-top" src="https://picsum.photos/200/300" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img class="card-img-top" src="https://picsum.photos/200/300" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img class="card-img-top" src="https://picsum.photos/200/300" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <img class="card-img-top" src="https://picsum.photos/200/300" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Card title</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="{{ url('js/typeahead.min.js') }}"></script>
    <script type="text/javascript">
      var companies = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
          url: '{{ route('search.companies.json') }}?query=%QUERY',
          wildcard: '%QUERY'
        }
      });

      $('#search input[name="search"]').typeahead(null, {
        name: 'companies',
        display: 'name',
        source: companies,
        templates: {
          suggestion: function(data){
            return '<p style="line-height: 40px"><img src="uploads/company/' + data.image + '" alt="Logo ' + data.name + '" style="max-width: 40px"> <strong style="padding-left:20px;">' + data.name + '</strong></p>';
          }
        }
      });
    </script>
@endsection