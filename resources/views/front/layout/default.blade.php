<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Etic - @yield('title')</title>
    <link rel="shortcut icon" href="{{ url('images/favicon.ico') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    @yield('css')
</head>
<body class="@if(empty($bodyClass)) gl-business-template gl-home-template @else {{ $bodyClass }} @endif">

    <div id="gl-circle-loader-wrapper">
        <div id="gl-circle-loader-center">
            <div class="gl-circle-load">
                <img src="{{ url('images/ploading.gif') }}" alt="Page Loader - ETIC">
            </div>
        </div>
    </div>

    @include('front.layout.menu')

    @yield('content')



    <footer>
    <div class="gl-footer-bottom-wrapper">
        <div class="container">
            <div class="row">
                <div class="gl-copyright-info-wrapper">
                    <p>Copyright &copy; 2019 Etic. Tous droits réservés</p>
                </div>

                <div class="gl-social-info-wrapper">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-behance"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-dribbble"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-vimeo"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/plugins.js') }}"></script>
<script src="{{ url('js/scripts.js') }}"></script>
<script src="{{ url('js/main.js') }}"></script>
@if(count($errors->get('name')) or count($errors->get('password')) or Session::has('wrong_password'))
    <script type="text/javascript">
        $('#gl-side-menu-btn').trigger('click');
    </script>
@endif
@yield('js')
</body>
</html>
