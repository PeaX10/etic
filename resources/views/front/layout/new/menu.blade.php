<nav class="navbar fixed-top navbar-expand-sm bg-transparent navbar-light">
    <div class="container">
        <a href="/" class="navbar-brand"><img src="{{ asset('images/logo_etic.png') }}" class="logo"> <span class="pl-3">etic</span></a>
        <div class="pl-auto">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar3">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbar3">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link font-weight-bold" href="#">A propos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link font-weight-bold" href="#">Nous contacter</a>
                    </li>
                    <li class="nav-item pl-3">
                        <button class="btn btn-warning bg-orange text-white font-weight-bold" href="#">Connexion</button>
                    </li>
                    <li class="nav-item pl-3">
                        <button class="btn btn-danger bg-red font-weight-bold" href="#">Enquêter</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>