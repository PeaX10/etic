@extends('front.layout.default')

@section('title', 'Réinitialiser mon mot de passe')

@section('content')
    <section class="gl-block-page-header gl-page-header-wrapper">
        <div class="container">
            <div class="row">
                <h1>Mot de passe oublié ?</h1>
            </div>
        </div>
    </section>
    <section class="gl-page-content-section" style="padding-top:0px">
        <div class="gl-inner-section">
            <div class="container">
                <div class="row">
                    <div class="gl-contact-form-wrapper col-md-8 col-md-offset-2">
                        @if(Session::has('error_user_not_exists'))
                            <div class="alert alert-danger">Il semblerait qu'aucun utilisateur n'ai été trouvé</div>
                        @elseif(Session::has('success_mail_sended'))
                            <div class="alert alert-success">Un mail avec un lien de réinitialisation vous a été envoyé !</div>
                        @else
                            <div class="alert alert-info">Un mail vous sera envoyer avec un lien pour réinitialiser votre mot de passe</div>
                        @endif
                        {!! BootForm::open() !!}
                        {!! BootForm::text('login', false, null, ['placeholder' => 'Nom d\'utilisateur ou email']) !!}
                        {!! BootForm::submit('Réinitialiser', ['class' => 'gl-btn pull-right', 'style' => 'margin-top:10px']) !!}
                        {!! BootForm::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection