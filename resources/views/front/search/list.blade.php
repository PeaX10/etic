@extends('front.layout.default', ['bodyClass' => 'gl-search-template gl-search-style-map-top gl-search-style-alt'])

@section('title', 'Résultat de votre recherche')

@section('css')
    <link rel="stylesheet" href="{{ url('css/custom.css') }}">
@endsection

@section('content')
    <section id="gl-result-section" class="gl-page-content-section">
        <div class="container">
            {!! BootForm::open(['method' => 'GET', 'class' => 'gl-contact-form']) !!}
            <div class="row">
                <div class="col-md-11">
                    <input type="text" name="keywords" placeholder="Mots clés" value="{{ $search }}">
                </div>
                <div class="col-md-1">
                    {!! BootForm::submit('Rechercher') !!}
                </div>
            </div>
            <hr>
            {!! BootForm::close() !!}
            <div class="row">
                <div class="gl-search-meta-wrapper">
                    <div class="gl-search-tags">
                        @foreach($keywords as $k => $keyword)
                            <a href="{{ route('search.unsetKeyword', ['search' => $search, 'unsetKeyword' => $k]) }}" class="gl-tag-btn gl-design">{{ $keyword }}</a>
                        @endforeach
                    </div>

                    <div class="gl-search-sorting">
                        <span>Trier par</span>

                        <div class="gl-sorting-dropdown">
                            <div class="gl-salary-sort">
                                <select class="gl-sort-selection">
                                    <option value="note">Note</option>
                                    <option value="name">Nom</option>
                                </select>
                            </div>

                            <div class="gl-date-sort">
                                <select class="gl-sort-selection">
                                    <option value="asc">Asc</option>
                                    <option value="desc">Desc</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="gl-search-result-wrapper">
                    <div class="gl-row">
                        <h3 class="gl-sub-heading">Résultats</h3>
                        @if($results->count())
                            @foreach($results as $company)
                                <div class="panel panel-default gl-job-list-item">
                                <div class="gl-job-list-item-wrapper panel-heading">
                                    <a role="button" data-toggle="collapse" href="#" aria-expanded="true">
                                        <div class="gl-job-company-logo gl-job-item-part">
                                            <img src="{{ $company->getImage() }}" alt="Logo {{ $company->name }}" class="gl-lazy" style="max-width:100px; max-height: 50px ">
                                        </div>
                                        <div class="gl-job-position-company gl-job-item-part">
                                            <h3>
                                                {{ $company->name }}
                                            </h3>
                                            @if($company->parent)<p class="gl-company-name">{{ $company->parent->name }}</p>@endif
                                        </div>
                                        <div class="gl-job-availability gl-job-item-part">
                                            <span class="gl-item-status-label full-time-job">NOTE</span>
                                        </div>
                                        <div class="gl-job-sallery gl-job-item-part">
                                        </div>
                                        <div class="gl-job-item-part">
                                            <i class="fas fa-comments"></i>
                                            <span>{{ $company->comments->count() }}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                            {{ $results->links() }}
                        @else
                            <p>Aucun résultat pour votre recherche</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection