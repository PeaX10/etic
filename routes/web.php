<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('home');
Route::get('/home2', 'PageController@index2')->name('home2');
Route::get('/about', 'PageController@about')->name('about');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/search', 'SearchController@search')->name('search');
Route::get('/search/{search}/{unsetKeyword}', 'SearchController@unsetKeyword')->name('search.unsetKeyword');
Route::get('/json/companies.json', 'SearchController@jsonCompanies')->name('search.companies.json');


Route::prefix('login')->group(function () {
    Route::post('/', 'LoginController@index')->name('login.post');
    Route::get('register', 'LoginController@register')->name('login.register');
    Route::get('forgot', 'LoginController@forgot')->name('login.forgot');
    Route::post('forgot', 'LoginController@postForgot')->name('login.forgot.post');
    Route::get('reset/{email}/{token}', 'LoginController@reset')->name('login.reset');
    Route::post('reset/{email}/{token}', 'LoginController@postReset')->name('login.reset.post');
    Route::post('register', 'LoginController@postRegister')->name('login.register.post');
    Route::get('logout', 'LoginController@logout')->name('login.logout');
    Route::get('facebook', 'LoginController@redirectToFacebook')->name('login.facebook');
    Route::get('facebook/callback', 'LoginController@handleProviderCallback')->name('login.facebook.redirect');
});

Route::prefix('blog')->group(function(){
    Route::get('/', 'BlogController@index')->name('blog.index');
    Route::get('{slug}', 'BlogController@show')->name('blog.show');
    Route::post('{slug}', 'BlogController@postComment')->name('blog.comment.post');
});

Route::prefix('admin')->middleware('admin')->as('admin.')->group(function () {
    Route::get('/', 'Admin\PageController@index')->name('index');
    Route::resource('users', 'Admin\UserController');

    Route::prefix('blog')->as('blog.')->group(function () {
        Route::resource('articles', 'Admin\Blog\ArticleController');
        Route::resource('categories', 'Admin\Blog\CategoryController');
        Route::resource('tags', 'Admin\Blog\TagController');
        Route::resource('comments', 'Admin\Blog\CommentController')->except('create', 'store');
    });

    Route::prefix('categorization')->as('categorization.')->group(function () {
        Route::resource('domains', 'Admin\Categorization\DomainController')->except('show');
        Route::resource('activity_areas', 'Admin\Categorization\ActivityAreaController')->except('show');
    });

    Route::prefix('industry')->as('industry.')->group(function () {
        Route::resource('companies', 'Admin\Industry\CompanyController');
        Route::resource('comments', 'Admin\Industry\CommentController');
    });

    Route::prefix('question')->as('question.')->group(function () {
        Route::resource('domains', 'Admin\Question\DomainController');
        Route::resource('activity_areas', 'Admin\Question\ActivityAreaController');
    });

    Route::get('algorithme', 'Admin\PageController@algorithme')->name('algorithme');
    Route::post('algorithme', 'Admin\PageController@postAlgorithme')->name('algorithme.post');

    Route::resource('icons', 'Admin\IconController')->except('show');

    Route::prefix('json')->as('json.')->group(function () {
        Route::get('tags', 'Admin\Blog\TagController@json')->name('tags');
        Route::get('wikipedia/{object}', 'Admin\PageController@wikipediaApi')->name('wikiApi');
    });

});

